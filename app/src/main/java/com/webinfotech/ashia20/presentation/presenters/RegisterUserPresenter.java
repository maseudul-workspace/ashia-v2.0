package com.webinfotech.ashia20.presentation.presenters;

public interface RegisterUserPresenter {
    void registerUser(String name,
                      String email,
                      String mobile,
                      String DOB,
                      String gender,
                      String state,
                      String city,
                      String pin,
                      String address,
                      String password,
                      String confirmPassword);
    interface View {
        void onRegisterSuccess();
        void showLoader();
        void hideLoader();
    }
}
