package com.webinfotech.ashia20.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.executors.impl.ThreadExecutor;
import com.webinfotech.ashia20.domain.models.Product;
import com.webinfotech.ashia20.presentation.presenters.OrderHistoryPresenter;
import com.webinfotech.ashia20.presentation.presenters.impl.OrderHistoryPresenterImpl;
import com.webinfotech.ashia20.presentation.ui.adapters.OrdersAdapter;
import com.webinfotech.ashia20.presentation.ui.dialogs.BankDetailsDialog;
import com.webinfotech.ashia20.threading.MainThreadImpl;

public class OrderHistoryActivity extends AppCompatActivity implements OrderHistoryPresenter.View, BankDetailsDialog.Callback {

    @BindView(R.id.recycler_view_orders)
    RecyclerView recyclerViewOrders;
    OrderHistoryPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    BankDetailsDialog bankDetailsDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Orders");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setUpProgressDialog();
        setUpBankDetailsDialog();
        mPresenter.fetchOrderHistory();
    }

    private void initialisePresenter() {
        mPresenter = new OrderHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpBankDetailsDialog() {
        bankDetailsDialog = new BankDetailsDialog(this, this, this);
        bankDetailsDialog.setUpDialog();
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(OrdersAdapter ordersAdapter) {
        recyclerViewOrders.setAdapter(ordersAdapter);
        recyclerViewOrders.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showBankFormDialog() {
        bankDetailsDialog.showDialog();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onSubmitClicked(String name, String bankName, String ifscCode, String branchName, String accountNo) {
        mPresenter.submitBankDetails(name, bankName, branchName, accountNo, ifscCode);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}