package com.webinfotech.ashia20.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.webinfotech.ashia20.AndroidApplication;
import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.executors.impl.ThreadExecutor;
import com.webinfotech.ashia20.domain.models.ProductDetails;
import com.webinfotech.ashia20.domain.models.Size;
import com.webinfotech.ashia20.domain.models.UserInfo;
import com.webinfotech.ashia20.domain.models.testing.Image;
import com.webinfotech.ashia20.domain.models.testing.Product;
import com.webinfotech.ashia20.domain.models.testing.Review;
import com.webinfotech.ashia20.presentation.presenters.ProductDetailsPresenter;
import com.webinfotech.ashia20.presentation.presenters.impl.ProductDetailsPresenterImpl;
import com.webinfotech.ashia20.presentation.ui.adapters.ColorsAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.ImageZoomViewPagerAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.MainViewpagerAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.ProductDetailsViewpager;
import com.webinfotech.ashia20.presentation.ui.adapters.ProductListVerticalAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.ProductsHorizontalAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.ReviewsAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.SizeAdapter;
import com.webinfotech.ashia20.presentation.ui.bottomsheet.LoginBottomSheet;
import com.webinfotech.ashia20.presentation.ui.dialogs.ImagePreviewDialog;
import com.webinfotech.ashia20.threading.MainThreadImpl;
import com.webinfotech.ashia20.util.GlideHelper;

import java.util.ArrayList;

public class ProductDetailsActivity extends AppCompatActivity implements SizeAdapter.Callback, ProductDetailsViewpager.Callback, ProductDetailsPresenter.View, ColorsAdapter.Callback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsing_toolbar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.recycler_view_sizes)
    RecyclerView recyclerViewSizes;
    @BindView(R.id.recycler_view_colors)
    RecyclerView recyclerViewColors;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.dot_layout)
    LinearLayout dotsLayout;
    @BindView(R.id.recycler_view_related_products)
    RecyclerView recyclerViewRelatedProducts;
    SizeAdapter sizeAdapter;
    ImagePreviewDialog imagePreviewDialog;
    @BindView(R.id.txt_view_product_name)
    TextView txtViewProductName;
    @BindView(R.id.txt_view_brand)
    TextView txtViewBrand;
    @BindView(R.id.txt_view_price)
    TextView txtViewPrice;
    @BindView(R.id.txt_view_mrp)
    TextView txtViewMrp;
    @BindView(R.id.txt_view_product_info)
    TextView txtViewProductInfo;
    @BindView(R.id.txt_view_more_info)
    TextView txtViewMoreInfo;
    @BindView(R.id.txt_view_discount_percentage)
    TextView txtViewDiscountPercentage;
    @BindView(R.id.btn_add_to_cart)
    Button btnAddToCart;
    @BindView(R.id.btn_go_to_bag)
    Button btnGoToBag;
    @BindView(R.id.btn_add_wishlist)
    Button btnAddToWishlist;
    @BindView(R.id.btn_wishlisted)
    Button btnWishlisted;
    ProductDetailsPresenterImpl mPresenter;
    Size[] sizes;
    com.webinfotech.ashia20.domain.models.Color[] colors;
    ColorsAdapter colorsAdapter;
    ProgressDialog progressDialog;
    int productId;
    int sizeId = 0;
    String colorHexCode = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        productId = getIntent().getIntExtra("productId", 0);
        setSupportActionBar(toolbar);
        setUpImagePreviewDialog();
        collapsingToolbarLayout.setTitle("Product Details");
        collapsingToolbarLayout.setExpandedTitleColor(Color.TRANSPARENT);
        collapsingToolbarLayout.setCollapsedTitleTextColor(Color.rgb(0, 0, 0));
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchProductDetails(productId);
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new ProductDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setUpImagePreviewDialog() {
        imagePreviewDialog = new ImagePreviewDialog(this, this);
        imagePreviewDialog.setUpDialog();
    }

    @Override
    public void loadData(ProductDetails productDetails, ProductsHorizontalAdapter adapter) {
        Log.e("LogMsg", "Brand Name: " + productDetails.brandName);
        if (productDetails.isWishList) {
            btnWishlisted.setVisibility(View.VISIBLE);
        } else {
            btnAddToWishlist.setVisibility(View.VISIBLE);
        }
        txtViewBrand.setText(productDetails.brandName);
        txtViewProductName.setText(productDetails.name);
        txtViewProductInfo.setText(productDetails.shortDescription);
        txtViewMoreInfo.setText(HtmlCompat.fromHtml(String.valueOf(HtmlCompat.fromHtml(productDetails.description, HtmlCompat.FROM_HTML_MODE_COMPACT)), HtmlCompat.FROM_HTML_MODE_COMPACT));
        sizes = productDetails.sizes;
        colors = productDetails.colors;
        for (int i = 0; i < sizes.length; i++) {
            if (sizes[i].stock > 0) {
                sizes[i].isSelected = true;
                sizeId = sizes[i].id;
                txtViewPrice.setText("₹ " + sizes[i].price);
                int productDiscount = (int) (100 -  sizes[i].price/sizes[0].mrp * 100);
                txtViewDiscountPercentage.setText(productDiscount + "% off");
                txtViewMrp.setText("₹ " + sizes[i].mrp);
                break;
            }
        }
        sizeAdapter = new SizeAdapter(this, sizes, this);
        recyclerViewSizes.setAdapter(sizeAdapter);
        recyclerViewSizes.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        colorsAdapter = new ColorsAdapter(this, productDetails.colors, this);
        recyclerViewColors.setAdapter(colorsAdapter);
        recyclerViewColors.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        ProductDetailsViewpager detailsViewpager = new ProductDetailsViewpager(this, productDetails.images, this);
        viewPager.setAdapter(detailsViewpager);
        prepareDotsIndicator(0, productDetails.images.length);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareDotsIndicator(position, productDetails.images.length);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        ImageZoomViewPagerAdapter imageZoomViewPagerAdapter = new ImageZoomViewPagerAdapter(this, productDetails.images);
        imagePreviewDialog.setViewPager2(imageZoomViewPagerAdapter, productDetails.images.length);
        recyclerViewRelatedProducts.setAdapter(adapter);
        recyclerViewRelatedProducts.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
    }

    private void prepareDotsIndicator(int sliderPosition, int length) {

        if(dotsLayout.getChildCount() > 0){
            dotsLayout.removeAllViews();
        }
        ImageView dots[] = new ImageView[length];
        for(int i = 0; i < length; i++) {
            dots[i] = new ImageView(this);
            if(i == sliderPosition) {
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            } else {
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(7, 0, 7, 0);
            dotsLayout.addView(dots[i], layoutParams);
        }

    }

    @OnClick(R.id.btn_add_to_cart) void onAddToCartClicked() {
        if (sizeId == 0) {
            Toasty.warning(this, "No Size Selected", Toast.LENGTH_SHORT).show();
        } else if (colorHexCode == null) {
            Toasty.warning(this, "Please Select a Color", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.addToCart(productId, 1, sizeId, colorHexCode);
        }
    }

    @OnClick(R.id.btn_go_to_bag) void onGoToBagClicked() {
        if (isLoggedIn()) {
            Intent intent = new Intent(this, CartListActivity.class);
            startActivity(intent);
        } else {
            showLoginBottomSheet();
        }
    }

    @Override
    public void onSizeSelected(int id) {
        for (int i = 0; i < sizes.length; i++) {
            if (sizes[i].id == id) {
                sizes[i].isSelected = true;
                txtViewPrice.setText("₹ " + sizes[i].price);
                int productDiscount = (int) (100 -  sizes[i].price/sizes[0].mrp * 100);
                txtViewDiscountPercentage.setText(productDiscount + "% off");
                txtViewMrp.setText("₹ " + sizes[i].mrp);
            } else {
                sizes[i].isSelected = false;
            }
        }
        sizeAdapter.updateDataSet(sizes);
        sizeId = id;
    }

    @Override
    public void onImageClicked(int position) {
        imagePreviewDialog.showDialog(position);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onColorChecked(int id) {
        for (int i = 0; i < colors.length; i++) {
            if (colors[i].id == id) {
                colors[i].isSelected = true;
                colorHexCode = colors[i].colorCode;
            } else {
                colors[i].isSelected = false;
            }
        }
        colorsAdapter.updateDataSet(colors);
    }

    @Override
    public void onAddToCartSuccess() {
        btnAddToCart.setVisibility(View.GONE);
        btnGoToBag.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAddToWishlistSuccess() {
        btnWishlisted.setVisibility(View.VISIBLE);
        btnAddToWishlist.setVisibility(View.GONE);
    }

    @Override
    public void onRemoveFromWishListSuccess() {
        btnWishlisted.setVisibility(View.GONE);
        btnAddToWishlist.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_wishlisted) void removeFromWishlist() {
        mPresenter.removeFromWishlist(productId);
    }

    @OnClick(R.id.btn_add_wishlist) void onAddToWishlistClicked() {
        if (isLoggedIn()) {
            mPresenter.addToWishList(productId);
        } else {
            showLoginBottomSheet();
        }
    }

    @OnClick(R.id.view_wishlist) void onViewWishlistClicked() {
        if (isLoggedIn()) {
            Intent intent = new Intent(this, WishlistActivity.class);
            startActivity(intent);
        } else {
            showLoginBottomSheet();
        }
    }

    private void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    private boolean isLoggedIn() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo != null) {
            Log.e("LogMsg", "User Id: " + userInfo.userId);
            Log.e("LogMsg", "Api token: " + userInfo.apiToken);
            return true;
        } else {
            return false;
        }
    }

}
