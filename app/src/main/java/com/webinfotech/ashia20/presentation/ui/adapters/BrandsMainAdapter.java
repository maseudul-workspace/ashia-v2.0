package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Brands;
import com.webinfotech.ashia20.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BrandsMainAdapter extends RecyclerView.Adapter<BrandsMainAdapter.ViewHolder> {

    Context mContext;
    Brands[] brands;

    public BrandsMainAdapter(Context mContext, Brands[] brands) {
        this.mContext = mContext;
        this.brands = brands;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_brands_main, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewBrandMain, mContext.getResources().getString(R.string.base_url) + "images/brands/" + brands[position].image, 10);
        holder.txtViewBrandName.setText(brands[position].name);
    }

    @Override
    public int getItemCount() {
        return brands.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_brand_main)
        ImageView imgViewBrandMain;
        @BindView(R.id.txt_view_brand_name)
        TextView txtViewBrandName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
