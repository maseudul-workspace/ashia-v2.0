package com.webinfotech.ashia20.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.webinfotech.ashia20.AndroidApplication;
import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.ApplyCouponCodeInteractor;
import com.webinfotech.ashia20.domain.interactors.FetchCartDetailsInteractor;
import com.webinfotech.ashia20.domain.interactors.FetchCouponsInteractor;
import com.webinfotech.ashia20.domain.interactors.FetchShippingChargesInteractor;
import com.webinfotech.ashia20.domain.interactors.RemoveCartItemInteractor;
import com.webinfotech.ashia20.domain.interactors.UpdateCartInteractor;
import com.webinfotech.ashia20.domain.interactors.impl.ApplyCouponCodeInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.FetchCartDetailsInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.FetchCouponsInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.FetchShippingChargesInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.RemoveCartItemInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.UpdateCartInteractorImpl;
import com.webinfotech.ashia20.domain.models.CartDetails;
import com.webinfotech.ashia20.domain.models.Coupon;
import com.webinfotech.ashia20.domain.models.Size;
import com.webinfotech.ashia20.domain.models.UserInfo;
import com.webinfotech.ashia20.domain.models.testing.Quantity;
import com.webinfotech.ashia20.presentation.presenters.CartDetailsPresenter;
import com.webinfotech.ashia20.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.ashia20.presentation.ui.adapters.CartListAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.CouponCodeAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.QuantityAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.SizeAdapter;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class CartDetailsPresenterImpl extends AbstractPresenter implements  CartDetailsPresenter,
                                                                            FetchCartDetailsInteractor.Callback,
                                                                            CartListAdapter.Callback,
                                                                            SizeAdapter.Callback,
                                                                            QuantityAdapter.Callback,
                                                                            UpdateCartInteractor.Callback,
                                                                            RemoveCartItemInteractor.Callback,
                                                                            FetchCouponsInteractor.Callback,
                                                                            ApplyCouponCodeInteractor.Callback,
                                                                            FetchShippingChargesInteractor.Callback
{

    Context mContext;
    CartDetailsPresenter.View mView;
    CartDetails[] cartDetails;
    ArrayList<Quantity> quantities;
    Size[] sizes;
    QuantityAdapter quantityAdapter;
    SizeAdapter sizeAdapter;
    int quantity;
    int sizeId;
    int cartId;

    public CartDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchCartDetails() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            FetchCartDetailsInteractorImpl fetchCartDetailsInteractor = new FetchCartDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchCartDetailsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void updateCart() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            UpdateCartInteractorImpl updateCartInteractor = new UpdateCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, cartId, sizeId, quantity);
            updateCartInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void fetchCoupons() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            FetchCouponsInteractorImpl fetchCouponsInteractor = new FetchCouponsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchCouponsInteractor.execute();
        }
    }

    @Override
    public void applyCoupon(String coupon) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            ApplyCouponCodeInteractorImpl applyCouponCodeInteractor = new ApplyCouponCodeInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, coupon);
            applyCouponCodeInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void fetchShippingCharges() {
        FetchShippingChargesInteractorImpl fetchShippingChargesInteractor = new FetchShippingChargesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchShippingChargesInteractor.execute();
    }

    @Override
    public void onGettingCartDetailsSuccess(CartDetails[] cartDetails) {
        if (cartDetails.length == 0) {
            mView.hideViews();
        }
        this.cartDetails = cartDetails;
        double mrp = 0;
        double price = 0;
        for (int i = 0; i < cartDetails.length; i++) {
            mrp = mrp + cartDetails[i].size.mrp;
            price = price + cartDetails[i].size.price;
        }
        CartListAdapter cartListAdapter = new CartListAdapter(mContext, cartDetails, this);
        mView.loadData(cartListAdapter, mrp, price);
        fetchShippingCharges();
        mView.hideLoader();
    }

    @Override
    public void onGettingCartDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.hideViews();
    }

    @Override
    public void onSizeClicked(CartDetails cartDetails) {
        cartId = cartDetails.id;
        quantity = cartDetails.quantity;
        sizeId = cartDetails.size.id;
        Size size = cartDetails.size;
        sizes = cartDetails.productDetails.sizes;
        for (int i = 0; i < sizes.length; i++) {
            if (sizes[i].id == size.id) {
                sizes[i].isSelected = true;
                break;
            }
        }
        sizeAdapter = new SizeAdapter(mContext, sizes, this);
        mView.loadSizeAdapter(sizeAdapter);
    }

    @Override
    public void onQuantityClicked(CartDetails cartDetails) {
        cartId = cartDetails.id;
        quantity = cartDetails.quantity;
        sizeId = cartDetails.size.id;
        quantities = new ArrayList<>();
        int limit;
        if (cartDetails.size.stock > 10) {
            limit = 10;
        } else {
            limit = cartDetails.size.stock;
        }
        for (int i = 1;  i <= limit; i++) {
            if (i == cartDetails.quantity) {
                Quantity quantity = new Quantity(i, Integer.toString(i), true);
                quantities.add(quantity);
            } else {
                Quantity quantity = new Quantity(i, Integer.toString(i), false);
                quantities.add(quantity);
            }
        }
        quantityAdapter = new QuantityAdapter(mContext, this, quantities);
        mView.loadQuantityAdapter(quantityAdapter);
    }

    @Override
    public void onRemoveClicked(int cartId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            RemoveCartItemInteractorImpl removeCartItemInteractor = new RemoveCartItemInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, cartId);
            removeCartItemInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onQtySelected(int id) {
        quantity = id;
        for (int i = 0; i < quantities.size(); i++) {
            if (quantities.get(i).id == id) {
                quantities.get(i).isSelected = true;
            } else {
                quantities.get(i).isSelected = false;
            }
        }
        quantityAdapter.updateDataSet(quantities);
    }

    @Override
    public void onSizeSelected(int id) {
        sizeId = id;
        for (int i = 0; i < sizes.length; i++) {
            if (sizes[i].id == id) {
                sizes[i].isSelected = true;
            } else {
                sizes[i].isSelected = false;
            }
        }
        sizeAdapter.updateDataSet(sizes);
    }

    @Override
    public void onUpdateCartSuccess() {
        fetchCartDetails();
    }

    @Override
    public void onUpdateCartFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRemoveCartItemSuccess() {
        fetchCartDetails();
    }

    @Override
    public void onRemoveCartItemFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCouponFetchSuccess(Coupon coupon) {
        mView.loadCouponData(coupon);
    }

    @Override
    public void onCouponFetchFail(String errorMsg, int loginError) {

    }

    @Override
    public void onCouponAppliedSuccess(Coupon coupon) {
        mView.hideLoader();
        mView.loadDiscount(coupon.discount, coupon.id);
    }

    @Override
    public void onCouponAppliedFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShippingChargeFetchSuccess(double minAmount, double shippingCharge) {
        mView.loadShippingCharges(minAmount, shippingCharge);
    }

    @Override
    public void onShippingChargeFetchFail() {

    }
}
