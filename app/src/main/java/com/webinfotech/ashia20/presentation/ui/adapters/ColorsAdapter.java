package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Color;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ColorsAdapter extends RecyclerView.Adapter<ColorsAdapter.ViewHolder> {

    public interface Callback {
        void onColorChecked(int id);
    }

    Context mContext;
    Color[] colors;
    Callback mCallback;

    public ColorsAdapter(Context mContext, Color[] colors, Callback mCallback) {
        this.mContext = mContext;
        this.colors = colors;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_colors, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Drawable background = holder.viewColor.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        gradientDrawable.setColor(android.graphics.Color.parseColor(colors[position].colorCode));
        if (colors[position].isSelected) {
            holder.layoutColorSelected.setVisibility(View.VISIBLE);
        } else {
            holder.layoutColorSelected.setVisibility(View.GONE);
        }
        holder.viewColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onColorChecked(colors[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return colors.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.view_color)
        View viewColor;
        @BindView(R.id.layout_color_selected)
        View layoutColorSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(Color[] colors) {
        this.colors = colors;
        notifyDataSetChanged();
    }

}
