package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Size;
import com.webinfotech.ashia20.domain.models.testing.Quantity;

import java.sql.Array;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class QuantityAdapter extends RecyclerView.Adapter<QuantityAdapter.ViewHolder> {

    public interface Callback {
        void onQtySelected(int id);
    }

    Context mContext;
    Callback mCallback;
    ArrayList<Quantity> quantities;

    public QuantityAdapter(Context mContext, Callback mCallback, ArrayList<Quantity> quantities) {
        this.mContext = mContext;
        this.mCallback = mCallback;
        this.quantities = quantities;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_quantity, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewQty.setText(quantities.get(position).qty);
        if (quantities.get(position).isSelected) {
            holder.layoutSelected.setVisibility(View.VISIBLE);
            holder.layoutNotSelected.setVisibility(View.GONE);
            holder.txtViewQty.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
        } else {
            holder.layoutSelected.setVisibility(View.GONE);
            holder.layoutNotSelected.setVisibility(View.VISIBLE);
            holder.txtViewQty.setTextColor(mContext.getResources().getColor(R.color.md_black_1000));
        }
        holder.layoutNotSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onQtySelected(quantities.get(position).id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return quantities.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_quantity)
        TextView txtViewQty;
        @BindView(R.id.layout_not_selected)
        View layoutNotSelected;
        @BindView(R.id.layout_selected)
        View layoutSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(ArrayList<Quantity> quantities) {
        this.quantities = quantities;
        notifyDataSetChanged();
    }

}
