package com.webinfotech.ashia20.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.ashia20.AndroidApplication;
import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.AddToWishlistInteractor;
import com.webinfotech.ashia20.domain.interactors.FetchWishlistInteractor;
import com.webinfotech.ashia20.domain.interactors.RemoveFromWishlistInteractor;
import com.webinfotech.ashia20.domain.interactors.impl.FetchWishlistInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.RemoveFromWishlistInteractorImpl;
import com.webinfotech.ashia20.domain.models.Product;
import com.webinfotech.ashia20.domain.models.UserInfo;
import com.webinfotech.ashia20.domain.models.Wishlist;
import com.webinfotech.ashia20.presentation.presenters.WishlistPresenter;
import com.webinfotech.ashia20.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.ashia20.presentation.ui.adapters.ProductListVerticalAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.WishlistAdapter;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class WishlistPresenterImpl extends AbstractPresenter implements WishlistPresenter,
                                                                        WishlistAdapter.Callback,
                                                                        FetchWishlistInteractor.Callback,
                                                                        RemoveFromWishlistInteractor.Callback
{

    Context mContext;
    WishlistPresenter.View mView;

    public WishlistPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }


    @Override
    public void fetchWishlist() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            FetchWishlistInteractorImpl fetchWishlistInteractor = new FetchWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchWishlistInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onProductClicked(int productId) {
        mView.onProductClicked(productId);
    }

    @Override
    public void removeFromWishlist(int productId, int position) {
        int wishListId = 0;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        Wishlist[] wishlists = androidApplication.getWishlists();
        for (int i = 0; i < wishlists.length; i++) {
            if (wishlists[i].product.id == productId) {
                wishListId = wishlists[i].wishListId;
                break;
            }
        }
        RemoveFromWishlistInteractorImpl removeFromWishlistInteractor = new RemoveFromWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, wishListId);
        removeFromWishlistInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onRemoveFromWishlistSuccess() {
        fetchWishlist();
    }

    @Override
    public void onRemoveFromWishlistFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onWishlistFetchSuccess(Wishlist[] wishlists) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishlists(wishlists);
        WishlistAdapter wishlistAdapter = new WishlistAdapter(mContext, wishlists, this);
        mView.loadAdapter(wishlistAdapter);
        mView.hideLoader();
        if (wishlists.length == 0) {
            mView.hideViews();
        }
    }

    @Override
    public void onWishlistFetchFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
