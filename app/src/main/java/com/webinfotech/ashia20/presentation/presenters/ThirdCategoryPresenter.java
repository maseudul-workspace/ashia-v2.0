package com.webinfotech.ashia20.presentation.presenters;

import com.webinfotech.ashia20.presentation.ui.adapters.ThirdCategoryAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.ThirdCategoryViewpagerAdapter;

public interface ThirdCategoryPresenter {
    void fetchThirdCategory(int subCatId);
    interface View {
        void loadAdapter(ThirdCategoryAdapter adapter, ThirdCategoryViewpagerAdapter viewpagerAdapter, int imageCount);
        void showLoader();
        void hideLoader();
    }
}
