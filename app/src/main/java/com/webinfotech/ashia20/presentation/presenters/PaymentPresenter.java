package com.webinfotech.ashia20.presentation.presenters;

import com.webinfotech.ashia20.domain.models.PaymentData;

public interface PaymentPresenter {
    void placeOrder(String couponId, int paymentType, int shippingAddressId);
    void verifyPayment( String razorpayOrderId,
                        String razorpayPaymentId,
                        String razorpaySignature,
                        int orderId);
    interface View {
        void goToOrderHistory();
        void initiatePayment(PaymentData paymentData, int orderId);
        void onPaymentVerificationSuccess();
        void showLoader();
        void hideLoader();
    }
}
