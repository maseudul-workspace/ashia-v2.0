package com.webinfotech.ashia20.presentation.presenters;

import com.webinfotech.ashia20.domain.models.Product;
import com.webinfotech.ashia20.domain.models.ProductDetails;
import com.webinfotech.ashia20.presentation.ui.adapters.ProductsHorizontalAdapter;

public interface ProductDetailsPresenter {
    void fetchProductDetails(int productId);
    void addToCart(int productId, int quantity, int sizeId, String colorCode);
    void fetchWishlist();
    void addToWishList(int productId);
    void removeFromWishlist(int productId);
    interface View {
        void loadData(ProductDetails productDetails, ProductsHorizontalAdapter adapter);
        void onAddToCartSuccess();
        void onAddToWishlistSuccess();
        void onRemoveFromWishListSuccess();
        void showLoader();
        void hideLoader();
    }
}
