package com.webinfotech.ashia20.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.ashia20.AndroidApplication;
import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.AddToWishlistInteractor;
import com.webinfotech.ashia20.domain.interactors.FetchProductListIntercator;
import com.webinfotech.ashia20.domain.interactors.FetchProductListWithFilterInteractor;
import com.webinfotech.ashia20.domain.interactors.FetchWishlistInteractor;
import com.webinfotech.ashia20.domain.interactors.RemoveFromWishlistInteractor;
import com.webinfotech.ashia20.domain.interactors.impl.AddToWishlistInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.FetchProductListInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.FetchProductListWithFilterInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.FetchWishlistInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.RemoveFromWishlistInteractorImpl;
import com.webinfotech.ashia20.domain.models.FilterName;
import com.webinfotech.ashia20.domain.models.FilterValue;
import com.webinfotech.ashia20.domain.models.Product;
import com.webinfotech.ashia20.domain.models.ProductListData;
import com.webinfotech.ashia20.domain.models.UserInfo;
import com.webinfotech.ashia20.domain.models.Wishlist;
import com.webinfotech.ashia20.presentation.presenters.ProductListPresenter;
import com.webinfotech.ashia20.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.ashia20.presentation.ui.adapters.FilterNameAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.ProductListVerticalAdapter;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class ProductListPresenterImpl extends AbstractPresenter implements  ProductListPresenter,
                                                                            FetchProductListWithFilterInteractor.Callback,
                                                                            ProductListVerticalAdapter.Callback,
                                                                            FilterNameAdapter.Callback,
                                                                            FetchProductListIntercator.Callback,
                                                                            AddToWishlistInteractor.Callback,
                                                                            RemoveFromWishlistInteractor.Callback,
                                                                            FetchWishlistInteractor.Callback
{

    Context mContext;
    ProductListPresenter.View mView;
    FetchProductListWithFilterInteractorImpl fetchProductListWithFilterInteractor;
    Product[] newProducts;
    ProductListVerticalAdapter productListVerticalAdapter;
    FilterName[] filterNames;
    FilterValue[] filterValues;
    FetchProductListInteractorImpl fetchProductListInteractor;
    int position;

    public ProductListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchProductListWithFilter(int categoryId, int type) {
        fetchProductListWithFilterInteractor = new FetchProductListWithFilterInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, categoryId, type);
        fetchProductListWithFilterInteractor.execute();
    }

    @Override
    public void fetchProductListWithoutFilter(int categoryId, int page, int type, ArrayList<Integer> brands, ArrayList<Integer> colors, ArrayList<Integer> sizes, int priceFrom, int priceTo, int sort, String refresh) {
        if (refresh.equals("refresh")) {
            newProducts = null;
        }
        fetchProductListInteractor = new FetchProductListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, categoryId, page, type, brands, colors, sizes, priceFrom, priceTo, sort);
        fetchProductListInteractor.execute();
    }

    @Override
    public void fetchWishlist() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            FetchWishlistInteractorImpl fetchWishlistInteractor = new FetchWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this,userInfo.apiToken, userInfo.userId);
            fetchWishlistInteractor.execute();
        }
    }

    @Override
    public void onGettingProductListWithFilterSuccess(ProductListData productListData, int totalPage) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        Wishlist[] wishlists = androidApplication.getWishlists();
        if (wishlists != null) {
            for (int i = 0; i < productListData.products.length; i++) {
                for (int j = 0; j < wishlists.length; j++) {
                    if (productListData.products[i].id == wishlists[j].product.id) {
                        productListData.products[i].isWishList = true;
                    }
                }
            }
        }
        Product[] tempProducts;
        tempProducts = newProducts;
        try {
            int len1 = tempProducts.length;
            int len2 = productListData.products.length;
            newProducts = new Product[len1 + len2];
            System.arraycopy(tempProducts, 0, newProducts, 0, len1);
            System.arraycopy(productListData.products, 0, newProducts, len1, len2);
            productListVerticalAdapter.updateData(newProducts);
        }catch (NullPointerException e){
            newProducts = productListData.products;
            filterNames = productListData.filterNames;
            for (int i = 0; i < filterNames.length; i++) {
                FilterValue[] filterValues = filterNames[i].filterValues;
                for (int j = 0; j < filterValues.length; j++) {
                    filterValues[j].filterNameId = filterNames[i].id;
                }
            }
            androidApplication.setFilterNames(filterNames);
            androidApplication.setPriceRange(productListData.priceRange);
            productListVerticalAdapter = new ProductListVerticalAdapter(mContext, productListData.products, this);
            mView.loadProductsWithFilter(productListVerticalAdapter, totalPage);
            mView.hideLoader();
        }



    }

    @Override
    public void onGettingProductListWithFilterFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onProductClicked(int productId) {
        mView.onProductClicked(productId);
    }

    @Override
    public void addToWishlist(int productId, int position) {
        this.position = position;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            mView.showLoginBottomSheet();
        } else {
            AddToWishlistInteractorImpl addToWishlistInteractor = new AddToWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, productId);
            addToWishlistInteractor.execute();
        }
    }

    @Override
    public void removeFromWishlist(int productId, int position) {
        this.position = position;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            mView.showLoginBottomSheet();
        } else {
            int wishListId = 0;
            this.position = position;
            Wishlist[] wishlists = androidApplication.getWishlists();
            for (int i = 0; i < wishlists.length; i++) {
                if (wishlists[i].product.id == productId) {
                    wishListId = wishlists[i].wishListId;
                    break;
                }
            }
            RemoveFromWishlistInteractorImpl removeFromWishlistInteractor = new RemoveFromWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, wishListId);
            removeFromWishlistInteractor.execute();
        }
    }

    @Override
    public void onFilterNameClicked(int filterNameId) {

    }

    @Override
    public void onGettingProductListSuccess(Product[] products, int totalPage) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        Wishlist[] wishlists = androidApplication.getWishlists();
        if (wishlists != null) {
            for (int i = 0; i < products.length; i++) {
                for (int j = 0; j < wishlists.length; j++) {
                    if (products[i].id == wishlists[j].product.id) {
                        products[i].isWishList = true;
                    }
                }
            }
        }
        Product[] tempProducts;
        tempProducts = newProducts;
        try {
            int len1 = tempProducts.length;
            int len2 = products.length;
            newProducts = new Product[len1 + len2];
            System.arraycopy(tempProducts, 0, newProducts, 0, len1);
            System.arraycopy(products, 0, newProducts, len1, len2);
            productListVerticalAdapter.updateData(newProducts);
        }catch (NullPointerException e) {
            newProducts = products;
            productListVerticalAdapter = new ProductListVerticalAdapter(mContext, products, this);
            mView.loadProductsWithFilter(productListVerticalAdapter, totalPage);
            mView.hideLoader();
        }
    }

    @Override
    public void onGettingProductListFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onAddToWishlistSuccess() {
        Toasty.normal(mContext, "Added To Wishlist").show();
        productListVerticalAdapter.onWishlistAddSuccess(position);
        fetchWishlist();
    }

    @Override
    public void onAddToWishlistFail(String errorMsg, int loginError) {
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onRemoveFromWishlistSuccess() {
        productListVerticalAdapter.onWishlistRemoveSuccess(position);
        fetchWishlist();
    }

    @Override
    public void onRemoveFromWishlistFail(String errorMsg, int loginError) {
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onWishlistFetchSuccess(Wishlist[] wishlists) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishlists(wishlists);
    }

    @Override
    public void onWishlistFetchFail(String errorMsg, int loginError) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishlists(null);
    }
}
