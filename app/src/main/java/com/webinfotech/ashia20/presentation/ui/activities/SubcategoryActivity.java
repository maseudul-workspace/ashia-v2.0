package com.webinfotech.ashia20.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.executors.impl.ThreadExecutor;
import com.webinfotech.ashia20.domain.models.testing.Category;
import com.webinfotech.ashia20.domain.models.testing.SubcategoryPromoImage;
import com.webinfotech.ashia20.presentation.presenters.SubcategoryPresenter;
import com.webinfotech.ashia20.presentation.presenters.impl.SubcategoryPresenterImpl;
import com.webinfotech.ashia20.presentation.ui.adapters.SubcategoryAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.ThirdCategoryAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.SubcategoryViewpagerAdapter;
import com.webinfotech.ashia20.threading.MainThreadImpl;
import com.webinfotech.ashia20.util.GlideHelper;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class SubcategoryActivity extends AppCompatActivity implements SubcategoryPresenter.View {

    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.dot_layout)
    LinearLayout dotsLayout;
    int currentPage = 0;
    @BindView(R.id.recycler_view_subcategory)
    RecyclerView recyclerViewSubcategory;
    int categoryId;
    ProgressDialog progressDialog;
    SubcategoryPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategory);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Subcategory");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        categoryId = getIntent().getIntExtra("categoryId", 0);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchSubcategories(categoryId);
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new SubcategoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void prepareDotsIndicator(int sliderPosition, int length){
        if(dotsLayout.getChildCount() > 0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[length];
        for(int i = 0; i < length; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(7, 0, 7, 0);
            dotsLayout.addView(dots[i], layoutParams);

        }
    }

    @Override
    public void loadAdapter(SubcategoryAdapter adapter, SubcategoryViewpagerAdapter viewpagerAdapter, int imageCount) {
        recyclerViewSubcategory.setAdapter(adapter);
        recyclerViewSubcategory.setLayoutManager(new LinearLayoutManager(this));
        viewPager.setAdapter(viewpagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareDotsIndicator(position, imageCount);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        prepareDotsIndicator(0, imageCount);

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToProductList(int categoryId, int type) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("categoryId", categoryId);
        intent.putExtra("type", type);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
