package com.webinfotech.ashia20.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.executors.impl.ThreadExecutor;
import com.webinfotech.ashia20.presentation.presenters.ShippingAddressPresenter;
import com.webinfotech.ashia20.presentation.presenters.impl.ShippingAddressPresenterImpl;
import com.webinfotech.ashia20.presentation.ui.adapters.ShippingAddressAdapter;
import com.webinfotech.ashia20.threading.MainThreadImpl;

public class SelectAddressActivity extends AppCompatActivity implements ShippingAddressPresenter.View {

    @BindView(R.id.recycler_view_address)
    RecyclerView recyclerViewAddress;
    ShippingAddressPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    double totalPrice;
    double totalMrp;
    String couponCode = null;
    int couponCodeDiscount = 0;
    double deliveryCharges = 0;
    int couponId = 0;
    int shippingAddressId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_address);
        ButterKnife.bind(this);
        totalPrice = getIntent().getDoubleExtra("totalPrice", 0);
        totalMrp = getIntent().getDoubleExtra("totalMrp", 0);
        deliveryCharges = getIntent().getDoubleExtra("deliveryCharges", 0);
        couponCode = getIntent().getStringExtra("couponCode");
        couponCodeDiscount = getIntent().getIntExtra("couponDiscount", 0);
        couponId = getIntent().getIntExtra("couponId", 0);
        getSupportActionBar().setTitle("Select Address");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new ShippingAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_add_address) void onAddAddressClicked() {
        Intent intent = new Intent(this, AddAddressActivity.class);
        startActivity(intent);
    }

    @Override
    public void loadAdapter(ShippingAddressAdapter adapter) {
        recyclerViewAddress.setVisibility(View.VISIBLE);
        recyclerViewAddress.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAddress.setAdapter(adapter);
    }

    @Override
    public void goToAddressDetails(int addressId) {
        Intent intent = new Intent(this, EditAddressActivity.class);
        intent.putExtra("addressId", addressId);
        startActivity(intent);
    }

    @Override
    public void onAddressSelected(int addressId) {
        shippingAddressId = addressId;
    }

    @Override
    public void onAddressFetchFailed() {
        recyclerViewAddress.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddress();
    }

    @OnClick(R.id.btn_continue) void onContinueClicked() {
        if (shippingAddressId == 0) {
            Toasty.warning(this, "Please Select A Shipping Address").show();
        } else {
            Intent intent = new Intent(this, PaymentActivity.class);
            intent.putExtra("totalMrp", totalMrp);
            intent.putExtra("totalPrice", totalPrice);
            intent.putExtra("deliveryCharges", deliveryCharges);
            intent.putExtra("couponCode", couponCode);
            intent.putExtra("couponDiscount", couponCodeDiscount);
            intent.putExtra("shippingAddressId", shippingAddressId);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}