package com.webinfotech.ashia20.presentation.presenters;

public interface PhoneNumberVerificationPresenter {
    void sendOtp(String phoneNo);
    interface View {
        void showLoader();
        void hideLoader();
        void onSendOtpSuccess(String otp);
    }
}
