package com.webinfotech.ashia20.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.executors.impl.ThreadExecutor;
import com.webinfotech.ashia20.presentation.presenters.WishlistPresenter;
import com.webinfotech.ashia20.presentation.presenters.impl.WishlistPresenterImpl;
import com.webinfotech.ashia20.presentation.ui.adapters.WishlistAdapter;
import com.webinfotech.ashia20.threading.MainThreadImpl;

public class WishlistActivity extends AppCompatActivity implements WishlistPresenter.View {

    @BindView(R.id.recycler_view_wishlist)
    RecyclerView recyclerViewWishlist;
    ProgressDialog progressDialog;
    WishlistPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Wishlist");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchWishlist();
    }

    private void initialisePresenter() {
        mPresenter = new WishlistPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(WishlistAdapter adapter) {
        recyclerViewWishlist.setAdapter(adapter);
        recyclerViewWishlist.setLayoutManager(new GridLayoutManager(this, 2));
    }

    @Override
    public void onProductClicked(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void hideViews() {
        recyclerViewWishlist.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}