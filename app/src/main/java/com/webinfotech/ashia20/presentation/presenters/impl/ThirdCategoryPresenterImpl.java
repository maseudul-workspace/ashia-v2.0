package com.webinfotech.ashia20.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.FetchThirdCategoryInteractor;
import com.webinfotech.ashia20.domain.interactors.impl.FetchThirdCategoryInteractorImpl;
import com.webinfotech.ashia20.domain.models.ThirdCategoryData;
import com.webinfotech.ashia20.presentation.presenters.ThirdCategoryPresenter;
import com.webinfotech.ashia20.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.ashia20.presentation.ui.adapters.ThirdCategoryAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.ThirdCategoryViewpagerAdapter;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ThirdCategoryPresenterImpl extends AbstractPresenter implements ThirdCategoryPresenter, FetchThirdCategoryInteractor.Callback, ThirdCategoryAdapter.Callback {

    Context mContext;
    ThirdCategoryPresenter.View mView;
    FetchThirdCategoryInteractorImpl fetchThirdCategoryInteractor;

    public ThirdCategoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchThirdCategory(int subCatId) {
        fetchThirdCategoryInteractor = new FetchThirdCategoryInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), subCatId);
        fetchThirdCategoryInteractor.execute();
    }

    @Override
    public void onGettingDataSuccess(ThirdCategoryData thirdCategoryData) {
        ThirdCategoryAdapter thirdCategoryAdapter = new ThirdCategoryAdapter(mContext, thirdCategoryData.thirdCategories, this);
        ThirdCategoryViewpagerAdapter viewpagerAdapter = new ThirdCategoryViewpagerAdapter(mContext, thirdCategoryData.images);
        mView.loadAdapter(thirdCategoryAdapter, viewpagerAdapter, thirdCategoryData.images.length);
        mView.hideLoader();
    }

    @Override
    public void onGettingDataFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onSubcategoryClicked(int id) {

    }
}
