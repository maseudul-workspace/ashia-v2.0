package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Subcategory;
import com.webinfotech.ashia20.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SubcategoryAdapter extends RecyclerView.Adapter<SubcategoryAdapter.ViewHolder> implements ThirdCategoryAdapter.Callback {

    @Override
    public void onSubcategoryClicked(int id) {
        mCallback.goToProductList(id, 2);
    }

    public interface Callback {
        void goToProductList(int id, int type);
    }

    Context mContext;
    Subcategory[] categories;
    Callback mCallback;

    public SubcategoryAdapter(Context mContext, Subcategory[] categories, Callback mCallback) {
        this.mContext = mContext;
        this.categories = categories;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_subcategory, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSubcategoryMain.setText(categories[position].name);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewSubcategoryMain, mContext.getResources().getString(R.string.base_url) + "images/category/sub_category/" + categories[position].image, 100);
        ThirdCategoryAdapter adapter = new ThirdCategoryAdapter(mContext, categories[position].thirdCategories, this);
        holder.recyclerViewSubcategory.setAdapter(adapter);
        holder.recyclerViewSubcategory.setLayoutManager(new GridLayoutManager(mContext, 3));
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.recyclerViewSubcategory.getVisibility() == View.VISIBLE) {
                    holder.recyclerViewSubcategory.setVisibility(View.GONE);
                } else {
                    holder.recyclerViewSubcategory.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_subcategory_main)
        TextView txtViewSubcategoryMain;
        @BindView(R.id.img_view_subcategory_main)
        ImageView imgViewSubcategoryMain;
        @BindView(R.id.recycler_view_subcategory)
        RecyclerView recyclerViewSubcategory;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
