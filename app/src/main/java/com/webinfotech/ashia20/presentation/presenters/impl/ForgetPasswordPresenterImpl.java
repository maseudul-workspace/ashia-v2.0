package com.webinfotech.ashia20.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.ForgetPasswordInteractor;
import com.webinfotech.ashia20.domain.interactors.impl.ForgetPasswordInteractorImpl;
import com.webinfotech.ashia20.presentation.presenters.ForgetPasswordPresenter;
import com.webinfotech.ashia20.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ForgetPasswordPresenterImpl extends AbstractPresenter implements ForgetPasswordPresenter, ForgetPasswordInteractor.Callback {

    Context mContext;
    ForgetPasswordPresenter.View mView;

    public ForgetPasswordPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void forgetPassword(String mobile, String password) {
        ForgetPasswordInteractorImpl forgetPasswordInteractor = new ForgetPasswordInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, mobile, password);
        forgetPasswordInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onForgetPasswordSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Password Changed").show();
    }

    @Override
    public void onForgetPasswordFail(String errorMsg) {
        mView.hideLoader();
        Toasty.normal(mContext, errorMsg).show();
    }
}
