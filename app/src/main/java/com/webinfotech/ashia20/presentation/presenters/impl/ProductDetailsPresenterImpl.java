package com.webinfotech.ashia20.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.ashia20.AndroidApplication;
import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.AddToCartInteractor;
import com.webinfotech.ashia20.domain.interactors.AddToWishlistInteractor;
import com.webinfotech.ashia20.domain.interactors.FetchProductDetailsInteractor;
import com.webinfotech.ashia20.domain.interactors.FetchWishlistInteractor;
import com.webinfotech.ashia20.domain.interactors.RemoveFromWishlistInteractor;
import com.webinfotech.ashia20.domain.interactors.impl.AddToCartInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.AddToWishlistInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.FetchProductDetailsInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.FetchWishlistInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.RemoveFromWishlistInteractorImpl;
import com.webinfotech.ashia20.domain.models.ProductDetails;
import com.webinfotech.ashia20.domain.models.ProductDetailsData;
import com.webinfotech.ashia20.domain.models.UserInfo;
import com.webinfotech.ashia20.domain.models.Wishlist;
import com.webinfotech.ashia20.presentation.presenters.ProductDetailsPresenter;
import com.webinfotech.ashia20.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.ashia20.presentation.ui.adapters.MainViewpagerAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.ProductsHorizontalAdapter;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ProductDetailsPresenterImpl extends AbstractPresenter implements   ProductDetailsPresenter,
                                                                                FetchProductDetailsInteractor.Callback,
                                                                                ProductsHorizontalAdapter.Callback,
                                                                                AddToCartInteractor.Callback,
                                                                                FetchWishlistInteractor.Callback,
                                                                                AddToWishlistInteractor.Callback,
                                                                                RemoveFromWishlistInteractor.Callback
{

    Context mContext;
    ProductDetailsPresenter.View mView;
    AddToCartInteractorImpl addToCartInteractor;

    public ProductDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchProductDetails(int productId) {
        FetchProductDetailsInteractorImpl fetchProductDetailsInteractor = new FetchProductDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, productId);
        fetchProductDetailsInteractor.execute();
    }

    @Override
    public void addToCart(int productId, int quantity, int sizeId, String colorCode) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {

        } else {
            AddToCartInteractorImpl addToCartInteractor = new AddToCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, productId, quantity, sizeId, colorCode);
            addToCartInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void fetchWishlist() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {

        } else {
            FetchWishlistInteractorImpl fetchWishlistInteractor = new FetchWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchWishlistInteractor.execute();
        }
    }

    @Override
    public void addToWishList(int productId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {

        } else {
            AddToWishlistInteractorImpl addToWishlistInteractor = new AddToWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, productId);
            addToWishlistInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void removeFromWishlist(int productId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {

        } else {
            int wishListId = 0;
            Wishlist[] wishlists = androidApplication.getWishlists();
            for (int i = 0; i < wishlists.length; i++) {
                if (wishlists[i].product.id == productId) {
                    wishListId = wishlists[i].wishListId;
                    break;
                }
            }
            RemoveFromWishlistInteractorImpl removeFromWishlistInteractor = new RemoveFromWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, wishListId);
            removeFromWishlistInteractor.execute();
        }
    }

    @Override
    public void onGettingProductDetailsSuccess(ProductDetailsData productDetailsData) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        Wishlist[] wishlists = androidApplication.getWishlists();
        for (int i = 0; i < wishlists.length; i++) {
            if (wishlists[i].product.id == productDetailsData.productDetails.id) {
                productDetailsData.productDetails.isWishList = true;
                break;
            }
        }
        ProductsHorizontalAdapter adapter = new ProductsHorizontalAdapter(mContext, productDetailsData.relatedProducts, this);
        mView.loadData(productDetailsData.productDetails, adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingProductDetailsFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onProductClicked(int productId) {

    }

    @Override
    public void onAddToCartSuccess() {
        mView.hideLoader();
        mView.onAddToCartSuccess();
    }

    @Override
    public void onAddToCartFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onAddToWishlistSuccess() {
        mView.hideLoader();
        Toasty.normal(mContext, "Added To Wishlist").show();
        mView.onAddToWishlistSuccess();
        fetchWishlist();
    }

    @Override
    public void onAddToWishlistFail(String errorMsg, int loginError) {
        Toasty.warning(mContext, errorMsg).show();
        mView.hideLoader();
    }

    @Override
    public void onRemoveFromWishlistSuccess() {
        mView.hideLoader();
        mView.onRemoveFromWishListSuccess();
        fetchWishlist();
    }

    @Override
    public void onRemoveFromWishlistFail(String errorMsg, int loginError) {
        Toasty.warning(mContext, errorMsg).show();
        mView.hideLoader();
    }

    @Override
    public void onWishlistFetchSuccess(Wishlist[] wishlists) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishlists(wishlists);
    }

    @Override
    public void onWishlistFetchFail(String errorMsg, int loginError) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishlists(null);
    }
}
