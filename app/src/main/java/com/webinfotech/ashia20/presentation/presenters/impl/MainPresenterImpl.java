package com.webinfotech.ashia20.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.webinfotech.ashia20.AndroidApplication;
import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.FetchHomeDataInteractor;
import com.webinfotech.ashia20.domain.interactors.FetchWishlistInteractor;
import com.webinfotech.ashia20.domain.interactors.impl.FetchHomeDataInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.FetchWishlistInteractorImpl;
import com.webinfotech.ashia20.domain.models.HomeData;
import com.webinfotech.ashia20.domain.models.UserInfo;
import com.webinfotech.ashia20.domain.models.Wishlist;
import com.webinfotech.ashia20.presentation.presenters.MainPresenter;
import com.webinfotech.ashia20.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.ashia20.presentation.ui.adapters.BrandsMainAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.CategoriesAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.CategoryMainAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.MainViewpagerAdapter;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
                                                                    FetchHomeDataInteractor.Callback,
                                                                    CategoriesAdapter.Callback,
                                                                    CategoryMainAdapter.Callback,
                                                                    FetchWishlistInteractor.Callback
{

    Context mContext;
    MainPresenter.View mView;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMainData() {
        Log.e("LogMsg", "Fetch Main Data");
        FetchHomeDataInteractorImpl fetchHomeDataInteractor = new FetchHomeDataInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        fetchHomeDataInteractor.execute();
    }

    @Override
    public void fetchWishlist() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
        } else {
            FetchWishlistInteractorImpl fetchWishlistInteractor = new FetchWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this,userInfo.apiToken, userInfo.userId);
            fetchWishlistInteractor.execute();
        }
    }

    @Override
    public void onGettingHomeDataSuccess(HomeData homeData) {
        CategoriesAdapter categoriesAdapter = new CategoriesAdapter(mContext, homeData.categories, this);
        MainViewpagerAdapter mainViewpagerAdapter = new MainViewpagerAdapter(mContext, homeData.sliders);
        BrandsMainAdapter brandsMainAdapter = new BrandsMainAdapter(mContext, homeData.brands);
        CategoryMainAdapter categoryMainAdapter = new CategoryMainAdapter(mContext, homeData.categories, this);
        mView.loadData(categoriesAdapter, homeData.banner.image, homeData.sliders.length, mainViewpagerAdapter, brandsMainAdapter, categoryMainAdapter);

    }

    @Override
    public void onGettingHomeDataFail(String errorMsg) {

    }

    @Override
    public void onCategoriesClicked(int categoryId) {
        mView.onCategoryClicked(categoryId);
    }


    @Override
    public void goToThirdCategory(int id) {
        mView.goToThirdCategory(id);
    }

    @Override
    public void goToProductList(int id) {
        mView.goToProductList(id);
    }

    @Override
    public void onWishlistFetchSuccess(Wishlist[] wishlists) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishlists(wishlists);
    }

    @Override
    public void onWishlistFetchFail(String errorMsg, int loginError) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishlists(null);
    }
}
