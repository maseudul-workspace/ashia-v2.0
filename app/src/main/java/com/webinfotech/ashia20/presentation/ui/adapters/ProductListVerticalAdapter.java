package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Product;
import com.webinfotech.ashia20.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductListVerticalAdapter extends RecyclerView.Adapter<ProductListVerticalAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int productId);
        void addToWishlist(int productId, int position);
        void removeFromWishlist(int productId, int position);
    }

    Context mContext;
    Product[] products;
    Callback mCallback;

    public ProductListVerticalAdapter(Context mContext, Product[] products, Callback callback) {
        this.mContext = mContext;
        this.products = products;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_product_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewProductName.setText(products[position].name);
        holder.txtViewBrand.setText(products[position].brandName);
        holder.txtViewPrice.setText("₹ " + products[position].minPrice);
        int productDiscount = (int) (100 -  products[position].minPrice/products[position].mrp * 100);
        holder.txtViewDiscount.setText(productDiscount + "% off");
        holder.txtViewMrp.setText("₹ " + products[position].mrp);
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "images/products/" + products[position].mainImage);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(products[position].id);
            }
        });
        if (products[position].isWishList) {
            holder.imgViewBookmarkRed.setVisibility(View.VISIBLE);
            holder.imgViewBookmarkGrey.setVisibility(View.GONE);
        } else {
            holder.imgViewBookmarkGrey.setVisibility(View.VISIBLE);
            holder.imgViewBookmarkRed.setVisibility(View.GONE);
        }
        holder.imgViewBookmarkGrey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.addToWishlist(products[position].id, position);
            }
        });
        holder.imgViewBookmarkRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.removeFromWishlist(products[position].id, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.length;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.txt_view_discount)
        TextView txtViewDiscount;
        @BindView(R.id.txt_view_brand)
        TextView txtViewBrand;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.img_view_bookmark_grey)
        ImageView imgViewBookmarkGrey;
        @BindView(R.id.img_view_bookmark_red)
        ImageView imgViewBookmarkRed;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(Product[] products) {
        this.products = products;
        notifyDataSetChanged();
    }

    public void onWishlistAddSuccess(int position) {
        this.products[position].isWishList = true;
        notifyItemChanged(position);
    }

    public void onWishlistRemoveSuccess(int position) {
        this.products[position].isWishList = false;
        notifyItemChanged(position);
    }

}
