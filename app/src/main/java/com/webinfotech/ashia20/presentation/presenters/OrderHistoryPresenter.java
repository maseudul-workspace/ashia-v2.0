package com.webinfotech.ashia20.presentation.presenters;

import com.webinfotech.ashia20.presentation.ui.adapters.OrdersAdapter;

public interface OrderHistoryPresenter {
    void fetchOrderHistory();
    void submitBankDetails(String name, String bankName, String branchName, String accountNo, String ifscCode);
    interface View {
        void loadAdapter(OrdersAdapter ordersAdapter);
        void showBankFormDialog();
        void showLoader();
        void hideLoader();
    }
}
