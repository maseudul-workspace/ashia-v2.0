package com.webinfotech.ashia20.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.FetchSubcategoryInteractor;
import com.webinfotech.ashia20.domain.interactors.impl.FetchSubcategoryInteractorImpl;
import com.webinfotech.ashia20.domain.models.Category;
import com.webinfotech.ashia20.domain.models.Subcategory;
import com.webinfotech.ashia20.domain.models.SubcategoryData;
import com.webinfotech.ashia20.presentation.presenters.SubcategoryPresenter;
import com.webinfotech.ashia20.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.ashia20.presentation.ui.adapters.SubcategoryAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.SubcategoryViewpagerAdapter;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class SubcategoryPresenterImpl extends AbstractPresenter implements SubcategoryPresenter, FetchSubcategoryInteractor.Callback, SubcategoryAdapter.Callback {

    Context mContext;
    SubcategoryPresenter.View mView;
    FetchSubcategoryInteractorImpl fetchSubcategoryInteractor;

    public SubcategoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSubcategories(int categoryId) {
        fetchSubcategoryInteractor = new FetchSubcategoryInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), categoryId);
        fetchSubcategoryInteractor.execute();
    }

    @Override
    public void onGettingSubcategorySuccess(SubcategoryData subcategoryData) {
        SubcategoryAdapter subcategoryAdapter = new SubcategoryAdapter(mContext, subcategoryData.subcategories, this);
        SubcategoryViewpagerAdapter viewpagerAdapter = new SubcategoryViewpagerAdapter(mContext, subcategoryData.images);
        mView.loadAdapter(subcategoryAdapter, viewpagerAdapter, subcategoryData.images.length);
        mView.hideLoader();
    }

    @Override
    public void onGettingSubcategoryFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void goToProductList(int id, int type) {
        mView.goToProductList(id, type);
    }
}
