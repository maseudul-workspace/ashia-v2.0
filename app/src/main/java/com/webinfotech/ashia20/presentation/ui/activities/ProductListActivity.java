package com.webinfotech.ashia20.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.fonts.Font;
import android.graphics.fonts.FontFamily;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.slider.RangeSlider;
import com.webinfotech.ashia20.AndroidApplication;
import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.executors.impl.ThreadExecutor;
import com.webinfotech.ashia20.domain.models.PriceRange;
import com.webinfotech.ashia20.domain.models.testing.Product;
import com.webinfotech.ashia20.presentation.presenters.ProductListPresenter;
import com.webinfotech.ashia20.presentation.presenters.impl.MainPresenterImpl;
import com.webinfotech.ashia20.presentation.presenters.impl.ProductListPresenterImpl;
import com.webinfotech.ashia20.presentation.ui.adapters.ProductListVerticalAdapter;
import com.webinfotech.ashia20.presentation.ui.bottomsheet.LoginBottomSheet;
import com.webinfotech.ashia20.presentation.ui.bottomsheet.SortByBottomSheet;
import com.webinfotech.ashia20.presentation.ui.dialogs.FilterByDialog;
import com.webinfotech.ashia20.presentation.ui.dialogs.FilterByDialogFragment;
import com.webinfotech.ashia20.threading.MainThreadImpl;

import java.util.ArrayList;
import java.util.List;

public class ProductListActivity extends AppCompatActivity implements ProductListPresenter.View, FilterByDialogFragment.Callback {

    @BindView(R.id.recycler_view_products_list)
    RecyclerView recyclerViewProductList;
    DialogFragment dialogFragment;
    FragmentTransaction ft;
    ProgressDialog progressDialog;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;
    ProductListPresenterImpl mPresenter;
    int categoryId;
    int type;
    ArrayList<Integer> brands = new ArrayList<>();
    ArrayList<Integer> sizes = new ArrayList<>();
    ArrayList<Integer> colors = new ArrayList<>();
    int priceFrom;
    int priceTo;
    BottomSheetDialog sortBottomSheetDialog;
    TextView txtViewNewest;
    TextView txtViewNameAsc;
    TextView txtViewNameDesc;
    TextView txtViewPriceLowToHigh;
    TextView txtViewPriceHighToLow;
    int sort = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);
        categoryId = getIntent().getIntExtra("categoryId", 0);
        type = getIntent().getIntExtra("type", 0);
        Log.e("LogMsg", "Category Id: " + categoryId);
        Log.e("LogMsg", "Type Id: " + type);
        getSupportActionBar().setTitle("Product List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setSortByBottomSheet();
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchProductListWithFilter(categoryId, type);
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new ProductListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setSortByBottomSheet() {
        if (sortBottomSheetDialog == null) {
            sortBottomSheetDialog = new BottomSheetDialog(this);
            View view = LayoutInflater.from(this).inflate(R.layout.sort_by_bottomsheet, null);
            txtViewNewest = (TextView) view.findViewById(R.id.txt_view_new_products);
            txtViewNameAsc = (TextView) view.findViewById(R.id.txt_view_name_asc);
            txtViewNameDesc = (TextView) view.findViewById(R.id.txt_view_name_desc);
            txtViewPriceLowToHigh = (TextView) view.findViewById(R.id.txt_view_low_to_high);
            txtViewPriceHighToLow = (TextView) view.findViewById(R.id.txt_view_high_to_low);
            txtViewNewest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtViewNewest.setTextColor(getResources().getColor(R.color.colorAccent));
                    txtViewNameAsc.setTextColor(getResources().getColor(R.color.md_grey_800));
                    txtViewNameDesc.setTextColor(getResources().getColor(R.color.md_grey_800));
                    txtViewPriceHighToLow.setTextColor(getResources().getColor(R.color.md_grey_800));
                    txtViewPriceLowToHigh.setTextColor(getResources().getColor(R.color.md_grey_800));
                    sort = 1;
                    sortBottomSheetDialog.dismiss();
                    fetchProductListWithoutFilterWrapper();
                }
            });
            txtViewNameAsc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sort = 4;
                    txtViewNewest.setTextColor(getResources().getColor(R.color.md_grey_800));
                    txtViewNameAsc.setTextColor(getResources().getColor(R.color.colorAccent));
                    txtViewNameDesc.setTextColor(getResources().getColor(R.color.md_grey_800));                    txtViewPriceHighToLow.setTextColor(getResources().getColor(R.color.md_grey_800));
                    txtViewPriceLowToHigh.setTextColor(getResources().getColor(R.color.md_grey_800));
                    sortBottomSheetDialog.dismiss();
                    fetchProductListWithoutFilterWrapper();
                }
            });
            txtViewNameDesc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sort = 5;
                    txtViewNewest.setTextColor(getResources().getColor(R.color.md_grey_800));
                    txtViewNameAsc.setTextColor(getResources().getColor(R.color.md_grey_800));
                    txtViewNameDesc.setTextColor(getResources().getColor(R.color.colorAccent));                    txtViewPriceHighToLow.setTextColor(getResources().getColor(R.color.md_grey_800));
                    txtViewPriceLowToHigh.setTextColor(getResources().getColor(R.color.md_grey_800));
                    sortBottomSheetDialog.dismiss();
                    fetchProductListWithoutFilterWrapper();
                }
            });
            txtViewPriceLowToHigh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sort = 2;
                    txtViewNewest.setTextColor(getResources().getColor(R.color.md_grey_800));
                    txtViewNameAsc.setTextColor(getResources().getColor(R.color.md_grey_800));
                    txtViewNameDesc.setTextColor(getResources().getColor(R.color.md_grey_800));                    txtViewPriceHighToLow.setTextColor(getResources().getColor(R.color.md_grey_800));
                    txtViewPriceLowToHigh.setTextColor(getResources().getColor(R.color.colorAccent));
                    sortBottomSheetDialog.dismiss();
                    fetchProductListWithoutFilterWrapper();
                }
            });
            txtViewPriceHighToLow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sort = 3;
                    txtViewNewest.setTextColor(getResources().getColor(R.color.md_grey_800));
                    txtViewNameAsc.setTextColor(getResources().getColor(R.color.md_grey_800));
                    txtViewNameDesc.setTextColor(getResources().getColor(R.color.md_grey_800));                    txtViewPriceHighToLow.setTextColor(getResources().getColor(R.color.colorAccent));
                    txtViewPriceLowToHigh.setTextColor(getResources().getColor(R.color.md_grey_800));
                    sortBottomSheetDialog.dismiss();
                    fetchProductListWithoutFilterWrapper();
                }
            });
            sortBottomSheetDialog.setContentView(view);
        }
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.layout_sort) void onSortByClicked() {
        sortBottomSheetDialog.show();

    }

    @OnClick(R.id.layout_filter) void onFilterClicked() {
        ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        dialogFragment = new FilterByDialogFragment();
        dialogFragment.show(ft, "dialog");
    }

    @Override
    public void loadProductsWithFilter(ProductListVerticalAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new GridLayoutManager(this, 2);
        recyclerViewProductList.setAdapter(adapter);
        recyclerViewProductList.setLayoutManager(layoutManager);
        recyclerViewProductList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchProductListWithoutFilter(categoryId, pageNo, type, brands, colors, sizes, priceFrom, priceTo, sort, "");
                    }
                }
            }
        });

        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        PriceRange priceRange = androidApplication.getPriceRange();
        priceFrom = Math.round(priceRange.priceForm);
        priceTo = Math.round(priceRange.priceTo);

    }

    @Override
    public void onProductClicked(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onApplyClicked(ArrayList<Integer> brands, ArrayList<Integer> colors, ArrayList<Integer> sizes, int priceFrom, int priceTo) {
        this.brands = brands;
        this.colors = colors;
        this.sizes = sizes;
        this.priceTo = priceTo;
        this.priceFrom = priceFrom;
        Log.e("LogMsg", "Brands Length: " + this.brands.size());
        Log.e("LogMsg", "Colors Length: " + this.colors.size());
        Log.e("LogMsg", "Sizes Length: " + this.sizes.size());
        Log.e("LogMsg", "Price To: " + this.priceTo);
        Log.e("LogMsg", "Price From: " + this.priceFrom);
        dialogFragment.dismiss();
        fetchProductListWithoutFilterWrapper();

    }

    private void fetchProductListWithoutFilterWrapper() {
        pageNo = 1;
        totalPage = 1;
        isScrolling = false;
        mPresenter.fetchProductListWithoutFilter(categoryId, pageNo, type, brands, colors, sizes, priceFrom, priceTo, sort, "refresh");
        showLoader();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
