package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Image;
import com.webinfotech.ashia20.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class MainViewpagerAdapter extends PagerAdapter {

    Context mContext;
    Image[] images;

    public MainViewpagerAdapter(Context mContext, Image[] images) {
        this.mContext = mContext;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.viewpager_main_layout, container, false);
        ImageView imageView = (ImageView) layout.findViewById(R.id.img_view_viewpager);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imageView, mContext.getResources().getString(R.string.base_url) + "images/slider/app/" + images[position].image, 10);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
