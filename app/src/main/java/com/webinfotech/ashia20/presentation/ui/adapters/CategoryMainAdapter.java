package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Category;
import com.webinfotech.ashia20.domain.models.Subcategory;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryMainAdapter extends RecyclerView.Adapter<CategoryMainAdapter.ViewHolder> implements SubcategoryMainAdapter.Callback {

    @Override
    public void goToThirdCategory(int subcategoryId) {
        mCallback.goToThirdCategory(subcategoryId);
    }

    @Override
    public void goToProductList(int subcategoryid) {
        mCallback.goToProductList(subcategoryid);
    }

    public interface Callback {
        void goToThirdCategory(int id);
        void goToProductList(int id);
    }

    Context mContext;
    Category[] categories;
    Callback mCallback;

    public CategoryMainAdapter(Context mContext, Category[] categories, Callback mCallback) {
        this.mContext = mContext;
        this.categories = categories;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_category_main, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewCategoryName.setText(categories[position].name);
        SubcategoryMainAdapter subcategoryMainAdapter = new SubcategoryMainAdapter(mContext, categories[position].subcategories, this);
        holder.recyclerViewSubcategory.setLayoutManager(new GridLayoutManager(mContext, 3));
        holder.recyclerViewSubcategory.setAdapter(subcategoryMainAdapter);
    }

    @Override
    public int getItemCount() {
        return categories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_category_name)
        TextView txtViewCategoryName;
        @BindView(R.id.recycler_view_subcategory_main)
        RecyclerView recyclerViewSubcategory;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
