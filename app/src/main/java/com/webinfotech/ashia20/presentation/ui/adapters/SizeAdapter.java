package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Size;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SizeAdapter extends RecyclerView.Adapter<SizeAdapter.ViewHolder> {

    public interface Callback {
        void onSizeSelected(int id);
    }

    Context mContext;
    Size[] sizes;
    Callback mCallback;

    public SizeAdapter(Context mContext, Size[] sizes, Callback callback) {
        this.mContext = mContext;
        this.sizes = sizes;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_size, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSizeSelected.setText(sizes[position].name);
        holder.txtViewSizeNotSelected.setText(sizes[position].name);
        if (sizes[position].isSelected) {
            holder.txtViewSizeSelected.setVisibility(View.VISIBLE);
            holder.txtViewSizeNotSelected.setVisibility(View.GONE);
        } else {
            holder.txtViewSizeSelected.setVisibility(View.GONE);
            holder.txtViewSizeNotSelected.setVisibility(View.VISIBLE);
        }
        holder.txtViewSizeNotSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sizes[position].stock > 0) {
                    mCallback.onSizeSelected(sizes[position].id);
                }
            }
        });
        if (sizes[position].stock < 1) {
            holder.txtViewStockStatus.setText("Out Of Stock");
        } else if (sizes[position].stock > 1 && sizes[position].stock <= 5) {
            holder.txtViewStockStatus.setText("Only " + sizes[position].stock + " Items Left");
        } else {
            holder.txtViewStockStatus.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return sizes.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_size_selected)
        TextView txtViewSizeSelected;
        @BindView(R.id.txt_view_size_not_selected)
        TextView txtViewSizeNotSelected;
        @BindView(R.id.txt_view_stock_status)
        TextView txtViewStockStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(Size[] sizes) {
        this.sizes = sizes;
        notifyDataSetChanged();
    }

}
