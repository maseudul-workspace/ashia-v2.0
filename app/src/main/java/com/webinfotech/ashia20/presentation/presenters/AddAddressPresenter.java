package com.webinfotech.ashia20.presentation.presenters;

public interface AddAddressPresenter {
    void addAddress(String name,
                    String email,
                    String mobile,
                    String city,
                    String state,
                    String pin,
                    String address);
    interface View {
        void onAddAddressSuccess();
        void showLoader();
        void hideLoader();
    }
}
