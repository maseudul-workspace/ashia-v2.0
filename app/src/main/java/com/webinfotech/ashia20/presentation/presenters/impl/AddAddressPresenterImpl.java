package com.webinfotech.ashia20.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.webinfotech.ashia20.AndroidApplication;
import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.AddShippingAddressInteractor;
import com.webinfotech.ashia20.domain.interactors.impl.AddShippingAddressInteractorImpl;
import com.webinfotech.ashia20.domain.models.UserInfo;
import com.webinfotech.ashia20.presentation.presenters.AddAddressPresenter;
import com.webinfotech.ashia20.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class AddAddressPresenterImpl extends AbstractPresenter implements AddAddressPresenter, AddShippingAddressInteractor.Callback {

    Context mContext;
    AddAddressPresenter.View mView;
    AddShippingAddressInteractorImpl addShippingAddressInteractor;

    public AddAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void addAddress(String name, String email, String mobile, String city, String state, String pin, String address) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            addShippingAddressInteractor = new AddShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken, name, email, mobile, state, city, pin, address);
            addShippingAddressInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onAddShippingAddressSuccess() {
        mView.hideLoader();
        mView.onAddAddressSuccess();
    }

    @Override
    public void onAddShippingAddressFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
