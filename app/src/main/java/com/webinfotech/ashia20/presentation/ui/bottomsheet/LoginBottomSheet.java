package com.webinfotech.ashia20.presentation.ui.bottomsheet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.presentation.ui.activities.LoginActivity;
import com.webinfotech.ashia20.presentation.ui.activities.RegisterActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginBottomSheet extends BottomSheetDialogFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_bottom_sheet, container, false);
    }
    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @OnClick(R.id.btn_login) void onLoginClicked() {
        dismiss();
        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_sign_up) void onSignUpClicked() {
        dismiss();
        Intent intent = new Intent(getContext(), RegisterActivity.class);
        startActivity(intent);
    }

}
