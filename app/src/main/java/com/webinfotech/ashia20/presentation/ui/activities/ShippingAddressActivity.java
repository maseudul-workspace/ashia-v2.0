package com.webinfotech.ashia20.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.executors.impl.ThreadExecutor;
import com.webinfotech.ashia20.domain.models.testing.Address;
import com.webinfotech.ashia20.presentation.presenters.ShippingAddressPresenter;
import com.webinfotech.ashia20.presentation.presenters.impl.ShippingAddressPresenterImpl;
import com.webinfotech.ashia20.presentation.ui.adapters.ShippingAddressAdapter;
import com.webinfotech.ashia20.threading.MainThreadImpl;

import java.util.ArrayList;

public class ShippingAddressActivity extends AppCompatActivity implements ShippingAddressPresenter.View {

    @BindView(R.id.recycler_view_address)
    RecyclerView recyclerViewAddress;
    ShippingAddressPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_address);
        getSupportActionBar().setTitle("ADDRESS");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new ShippingAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.txt_view_add_address) void onAddAddressClicked() {
        Intent intent = new Intent(this, AddAddressActivity.class);
        startActivity(intent);
    }

    @Override
    public void loadAdapter(ShippingAddressAdapter adapter) {
        recyclerViewAddress.setVisibility(View.VISIBLE);
        recyclerViewAddress.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAddress.setAdapter(adapter);
    }

    @Override
    public void goToAddressDetails(int addressId) {
        Intent intent = new Intent(this, EditAddressActivity.class);
        intent.putExtra("addressId", addressId);
        startActivity(intent);
    }

    @Override
    public void onAddressSelected(int addressId) {

    }

    @Override
    public void onAddressFetchFailed() {
        recyclerViewAddress.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddress();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
