package com.webinfotech.ashia20.presentation.presenters;

import com.webinfotech.ashia20.presentation.ui.adapters.BrandsMainAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.CategoriesAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.CategoryMainAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.MainViewpagerAdapter;

public interface MainPresenter {
    void fetchMainData();
    void fetchWishlist();
    interface View {
        void loadData(CategoriesAdapter categoriesAdapter, String bannerImage,int imageCount, MainViewpagerAdapter mainViewpagerAdapter, BrandsMainAdapter brandsMainAdapter, CategoryMainAdapter categoryMainAdapter);
        void goToThirdCategory(int subcategoryId);
        void goToProductList(int subcategoryId);
        void onCategoryClicked(int categoryId);
    }
}
