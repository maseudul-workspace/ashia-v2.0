package com.webinfotech.ashia20.presentation.presenters;

import com.webinfotech.ashia20.presentation.ui.adapters.SubcategoryAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.SubcategoryViewpagerAdapter;

public interface SubcategoryPresenter {
    void fetchSubcategories(int categoryId);
    interface View {
        void loadAdapter(SubcategoryAdapter adapter, SubcategoryViewpagerAdapter viewpagerAdapter, int imageCount);
        void showLoader();
        void hideLoader();
        void goToProductList(int categoryId, int type);
    }
}
