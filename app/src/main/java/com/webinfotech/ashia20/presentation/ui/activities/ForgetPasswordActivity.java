package com.webinfotech.ashia20.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.executors.impl.ThreadExecutor;
import com.webinfotech.ashia20.presentation.presenters.ForgetPasswordPresenter;
import com.webinfotech.ashia20.presentation.presenters.impl.ForgetPasswordPresenterImpl;
import com.webinfotech.ashia20.threading.MainThreadImpl;

public class ForgetPasswordActivity extends AppCompatActivity implements ForgetPasswordPresenter.View {

    @BindView(R.id.txt_input_password_layout)
    TextInputLayout txtInputPasswordLayout;
    @BindView(R.id.txt_input_repeat_passowrd_layout)
    TextInputLayout txtInputRepeatPasswordLayout;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.edit_text_repeat_password)
    EditText editTextRepeatPassword;
    ProgressDialog progressDialog;
    String phoneNo;
    ForgetPasswordPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Create Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        phoneNo = getIntent().getStringExtra("phoneNo");
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new ForgetPasswordPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        txtInputPasswordLayout.setError("");
        txtInputRepeatPasswordLayout.setError("");
        if (editTextPassword.getText().toString().trim().isEmpty()) {
            txtInputPasswordLayout.setError("* Enter Password");
        } else if (editTextRepeatPassword.getText().toString().trim().isEmpty()) {
            txtInputRepeatPasswordLayout.setError("* Repeat Password");
        } else if (!editTextPassword.getText().toString().trim().equals(editTextRepeatPassword.getText().toString().trim())) {
            txtInputRepeatPasswordLayout.setError("Password Mismatch");
        } else {
            mPresenter.forgetPassword(phoneNo, editTextPassword.getText().toString());
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}