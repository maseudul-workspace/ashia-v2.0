package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.CartDetails;
import com.webinfotech.ashia20.domain.models.testing.Product;
import com.webinfotech.ashia20.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.ViewHolder> {

    public interface Callback {
        void onSizeClicked(CartDetails cartDetails);
        void onQuantityClicked(CartDetails cartDetails);
        void onRemoveClicked(int cartId);
    }

    Context mContext;
    CartDetails[] cartDetails;
    Callback mCallback;

    public CartListAdapter(Context mContext, CartDetails[] cartDetails, Callback mCallback) {
        this.mContext = mContext;
        this.cartDetails = cartDetails;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_cart_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewProductName.setText(cartDetails[position].productDetails.name);
        holder.txtViewBrand.setText(cartDetails[position].productDetails.brandName);
        holder.txtViewPrice.setText("₹ " + cartDetails[position].size.price);
        int productDiscount = (int) (100 -  cartDetails[position].size.price/cartDetails[position].size.mrp * 100);
        holder.txtViewDiscount.setText(productDiscount + "% off");
        holder.txtViewMrp.setText("₹ " + cartDetails[position].size.mrp);
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "images/products/" + cartDetails[position].productDetails.mainImage);
        holder.sizeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSizeClicked(cartDetails[position]);
            }
        });
        holder.qtyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onQuantityClicked(cartDetails[position]);
            }
        });
        holder.txtViewQty.setText("Qty: " + cartDetails[position].quantity);
        holder.txtViewSize.setText("Size: " + cartDetails[position].size.name);
        holder.layoutRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirmation dialog");
                builder.setMessage("You are about to remove a item from cart. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton(Html.fromHtml("<font color='#00BFA5'><b>Yes</b></font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onRemoveClicked(cartDetails[position].id);
                    }
                });

                builder.setNegativeButton(Html.fromHtml("<font color='#FF1744'><b>No</b></font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartDetails.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.txt_view_discount)
        TextView txtViewDiscount;
        @BindView(R.id.txt_view_brand)
        TextView txtViewBrand;
        @BindView(R.id.txt_view_size)
        TextView txtViewSize;
        @BindView(R.id.txt_view_qty)
        TextView txtViewQty;
        @BindView(R.id.size_layout)
        View sizeLayout;
        @BindView(R.id.qty_layout)
        View qtyLayout;
        @BindView(R.id.layout_remove)
        View layoutRemove;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
