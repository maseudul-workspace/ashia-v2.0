package com.webinfotech.ashia20.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Coupon;
import com.webinfotech.ashia20.presentation.ui.adapters.CouponCodeAdapter;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CouponCodeDialog {

    public interface Callback {
        void onApplyCouponClicked(String coupon);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.txt_view_coupon_code)
    TextView txtViewCouponCode;
    @BindView(R.id.txt_view_save_amt)
    TextView txtViewSaveAmt;
    @BindView(R.id.txt_view_coupon_desc)
    TextView txtViewCouponDesc;
    @BindView(R.id.main_layout)
    View mainLayout;
    String coupon;

    public CouponCodeDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = callback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_coupon_code_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setCouponData(Coupon coupon) {
        txtViewCouponCode.setText(coupon.coupon);
        txtViewCouponDesc.setText(coupon.description);
        txtViewSaveAmt.setText(coupon.discount + "% OFF On Total Amount");
        this.coupon = coupon.coupon;
        mainLayout.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.img_view_cancel) void onCancelClicked() {
        hideDialog();
    }

    @OnClick(R.id.btn_apply_coupon) void onApplyCouponClicked() {
        mCallback.onApplyCouponClicked(coupon);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
