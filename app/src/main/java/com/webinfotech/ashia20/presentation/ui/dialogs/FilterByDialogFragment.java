package com.webinfotech.ashia20.presentation.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.google.android.material.slider.RangeSlider;
import com.webinfotech.ashia20.AndroidApplication;
import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.FilterName;
import com.webinfotech.ashia20.domain.models.FilterValue;
import com.webinfotech.ashia20.domain.models.PriceRange;
import com.webinfotech.ashia20.presentation.ui.adapters.FilterNameAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.FilterValueAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FilterByDialogFragment extends DialogFragment implements FilterNameAdapter.Callback, FilterValueAdapter.Callback {

    public interface Callback {
       void onApplyClicked(ArrayList<Integer> brands, ArrayList<Integer> colors, ArrayList<Integer> sizes, int priceFrom, int priceTo);
    }

    @BindView(R.id.range_slider)
    RangeSlider rangeSlider;
    @BindView(R.id.txt_view_price_range)
    TextView txtViewPriceRange;
    @BindView(R.id.recycler_filter_name)
    RecyclerView recyclerViewFilterName;
    @BindView(R.id.recycler_view_filter_value)
    RecyclerView recyclerViewFilterValue;
    @BindView(R.id.price_range_layout)
    View priceRangeLayout;
    FilterNameAdapter filterNameAdapter;
    Context mContext;
    @BindView(R.id.txt_view_price)
    TextView txtViewPrice;
    FilterValueAdapter filterValueAdapter;
    FilterName[] filterNames;
    FilterValue[] filterValues;
    PriceRange priceRange;
    Callback mCallback;
    int priceFrom;
    int priceTo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_filter_dialog, container, false);
        // Do all the stuff to initialize your custom view
        ButterKnife.bind(this, v);
        rangeSlider.addOnChangeListener(new RangeSlider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull RangeSlider slider, float value, boolean fromUser) {
                List<Float> values = slider.getValues();
                txtViewPriceRange.setText("₹" + Math.round(values.get(0)) + " - ₹" + Math.round(values.get(1)));
                priceFrom = Math.round(values.get(0));
                priceTo = Math.round(values.get(1));
            }
        });
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        filterNames = androidApplication.getFilterNames();
        priceRange = androidApplication.getPriceRange();
        priceFrom = Math.round(priceRange.priceForm);
        priceTo = Math.round(priceRange.priceTo);
        List<Float> prices = new ArrayList<>();
        prices.add(priceRange.priceTo);
        prices.add(priceRange.priceForm);
        txtViewPriceRange.setText("₹" + Math.round(priceRange.priceForm) + " - ₹" + Math.round(priceRange.priceTo));
        rangeSlider.setValues(prices);
        rangeSlider.setValueFrom(priceRange.priceForm);
        rangeSlider.setValueTo(priceRange.priceTo);
        priceRangeLayout.setVisibility(View.VISIBLE);
        recyclerViewFilterValue.setVisibility(View.GONE);
        txtViewPrice.setBackgroundColor(mContext.getResources().getColor(R.color.md_white_1000));
        for (int i = 0; i < filterNames.length; i++) {
            filterNames[i].isSelected = false;
        }
        filterNameAdapter = new FilterNameAdapter(mContext, filterNames, this);
        recyclerViewFilterName.setAdapter(filterNameAdapter);
        recyclerViewFilterName.setLayoutManager(new LinearLayoutManager(mContext));
        return v;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        if (context instanceof Callback)
        {
            mCallback = (Callback)context;
        }
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }


    @Override
    public void onFilterNameClicked(int filterNameId) {
        for (int i = 0; i < filterNames.length; i++) {
            if (filterNames[i].id == filterNameId) {
                filterValues = filterNames[i].filterValues;
                filterNames[i].isSelected = true;
            } else {
                filterNames[i].isSelected = false;
            }
        }
        filterNameAdapter.updateDataset(filterNames);
        filterValueAdapter = new FilterValueAdapter(mContext, filterValues, this);
        recyclerViewFilterValue.setAdapter(filterValueAdapter);
        recyclerViewFilterValue.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewFilterValue.setVisibility(View.VISIBLE);
        priceRangeLayout.setVisibility(View.GONE);
        txtViewPrice.setBackgroundColor(mContext.getResources().getColor(R.color.md_grey_200));
    }

    @Override
    public void onFilterValueClicked(int filterValueId, int filterNameId) {
        for (int i = 0; i < filterNames.length; i++) {
            if (filterNames[i].id == filterNameId) {
                filterValues = filterNames[i].filterValues;
                break;
            }
        }
        for (int i = 0; i < filterValues.length; i++) {
            if (filterValues[i].id == filterValueId) {
                filterValues[i].isSelected = !filterValues[i].isSelected;
                break;
            }
        }
        filterValueAdapter.updateDataset(filterValues);
    }

    @OnClick(R.id.txt_view_price) void onPriceFilterClicked() {
        priceRangeLayout.setVisibility(View.VISIBLE);
        recyclerViewFilterValue.setVisibility(View.GONE);
        txtViewPrice.setBackgroundColor(mContext.getResources().getColor(R.color.md_white_1000));
        for (int i = 0; i < filterNames.length; i++) {
            filterNames[i].isSelected = false;
        }
        filterNameAdapter.updateDataset(filterNames);
    }

    @OnClick(R.id.txt_view_dialog_close) void onCloseDialogClicked() {
        dismiss();
    }

    @OnClick(R.id.txt_view_apply_filter) void onApplyFilterClicked() {
        ArrayList<Integer> brands = new ArrayList<>();
        ArrayList<Integer> colors = new ArrayList<>();
        ArrayList<Integer> sizes = new ArrayList<>();
        for (int i = 0; i < filterNames.length; i++) {
            FilterValue[] filterValues = filterNames[i].filterValues;
            for (int j = 0; j < filterValues.length; j++) {
                if (filterValues[j].isSelected) {
                    switch (filterNames[i].id) {
                        case 1:
                            brands.add(filterValues[j].id);
                            break;
                        case 2:
                            colors.add(filterValues[j].id);
                            break;
                        case 3:
                            sizes.add(filterValues[j].id);
                            break;
                    }
                }
            }
        }
        mCallback.onApplyClicked(brands, colors, sizes, priceFrom, priceTo);
    }

}
