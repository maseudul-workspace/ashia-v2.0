package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Image;
import com.webinfotech.ashia20.domain.models.testing.SubcategoryPromoImage;
import com.webinfotech.ashia20.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class SubcategoryViewpagerAdapter extends PagerAdapter {

    Context mContext;
    Image[] subcategoryPromoImages;

    public SubcategoryViewpagerAdapter(Context mContext, Image[] subcategoryPromoImages) {
        this.mContext = mContext;
        this.subcategoryPromoImages = subcategoryPromoImages;
    }

    @Override
    public int getCount() {
        return subcategoryPromoImages.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.viewpager_subcategory_image_layout, container, false);
        ImageView imageView = (ImageView) layout.findViewById(R.id.img_view_viewpager);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imageView, mContext.getResources().getString(R.string.base_url) + "images/category/category/"  +subcategoryPromoImages[position].image, 10);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
