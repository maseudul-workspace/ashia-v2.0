package com.webinfotech.ashia20.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.ashia20.AndroidApplication;
import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.CancelOrderInteractor;
import com.webinfotech.ashia20.domain.interactors.FetchOrderHistoryInteractor;
import com.webinfotech.ashia20.domain.interactors.RequestRefundInteractor;
import com.webinfotech.ashia20.domain.interactors.RequestReturnRefundInteractor;
import com.webinfotech.ashia20.domain.interactors.impl.CancelOrderInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.FetchOrderHistoryInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.RequestRefundInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.RequestReturnRefundInteractorImpl;
import com.webinfotech.ashia20.domain.models.OrderProducts;
import com.webinfotech.ashia20.domain.models.Orders;
import com.webinfotech.ashia20.domain.models.UserInfo;
import com.webinfotech.ashia20.presentation.presenters.OrderHistoryPresenter;
import com.webinfotech.ashia20.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.ashia20.presentation.ui.adapters.OrdersAdapter;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class OrderHistoryPresenterImpl extends AbstractPresenter implements OrderHistoryPresenter,
                                                                            FetchOrderHistoryInteractor.Callback,
                                                                            OrdersAdapter.Callback,
                                                                            CancelOrderInteractor.Callback,
                                                                            RequestRefundInteractor.Callback,
                                                                            RequestReturnRefundInteractor.Callback
{

    Context mContext;
    OrderHistoryPresenter.View mView;
    Orders[] orders;
    int orderItemId;
    int orderStatus;

    public OrderHistoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchOrderHistory() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            FetchOrderHistoryInteractorImpl fetchOrderHistoryInteractor = new FetchOrderHistoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchOrderHistoryInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void submitBankDetails(String name, String bankName, String branchName, String accountNo, String ifscCode) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            if (orderStatus == 1) {
                RequestRefundInteractorImpl requestRefundInteractor = new RequestRefundInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, orderItemId, name, bankName, branchName, accountNo, ifscCode);
                requestRefundInteractor.execute();
                mView.showLoader();
            } else {
                RequestReturnRefundInteractorImpl requestReturnRefundInteractor = new RequestReturnRefundInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, orderItemId, name, bankName, branchName, accountNo, ifscCode);
                requestReturnRefundInteractor.execute();
                mView.showLoader();
            }
        }
    }

    @Override
    public void onGettingOrderHistorySuccess(Orders[] orders) {
        this.orders = orders;
        OrdersAdapter ordersAdapter = new OrdersAdapter(mContext, orders, this);
        mView.loadAdapter(ordersAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingOrderHistoryFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onCancelClicked(int orderItemId) {
        this.orderItemId = orderItemId;
        int paymentType = 0;
        for (int i = 0; i < orders.length; i++) {
            OrderProducts[] orderProducts = orders[i].orderProducts;
            for (int j = 0; j < orderProducts.length; j++) {
                if (orderProducts[j].orderItemId == orderItemId) {
                    paymentType = orders[i].paymentType;
                    break;
                }
            }
            if (paymentType > 0) {
                break;
            }
        }
        if (paymentType == 2) {
            orderStatus = 1;
            mView.showBankFormDialog();
        } else {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            UserInfo userInfo = androidApplication.getUserInfo(mContext);
            if (userInfo == null) {
                Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
            } else {
                CancelOrderInteractorImpl cancelOrderInteractor = new CancelOrderInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.apiToken, orderItemId);
                cancelOrderInteractor.execute();
                mView.showLoader();
            }
        }
    }

    @Override
    public void onReturnClicked(int itemOrderId) {
        this.orderItemId = itemOrderId;
        orderStatus = 2;
        mView.showBankFormDialog();
    }

    @Override
    public void onOrderCancelSuccess() {
        fetchOrderHistory();
    }

    @Override
    public void onOrderCancelFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onRequestRefundSuccess() {
        fetchOrderHistory();
    }

    @Override
    public void onRequestRefundFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onRequestReturnRefundSuccess() {
        fetchOrderHistory();
    }

    @Override
    public void onRequestReturnRefundFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
