package com.webinfotech.ashia20.presentation.presenters;

import com.webinfotech.ashia20.presentation.ui.adapters.WishlistAdapter;

public interface WishlistPresenter {
    void fetchWishlist();
    interface View {
        void loadAdapter(WishlistAdapter adapter);
        void onProductClicked(int productId);
        void hideViews();
        void showLoader();
        void hideLoader();
    }
}
