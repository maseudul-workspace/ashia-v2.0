package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.ThirdCategory;
import com.webinfotech.ashia20.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ThirdCategoryAdapter extends RecyclerView.Adapter<ThirdCategoryAdapter.ViewHolder> {

    public interface Callback {
        void onSubcategoryClicked(int id);
    }

    Context mContext;
    ThirdCategory[] thirdCategories;
    Callback mCallback;

    public ThirdCategoryAdapter(Context mContext, ThirdCategory[] thirdCategories, Callback callback) {
        this.mContext = mContext;
        this.thirdCategories = thirdCategories;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_third_category, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSubcategory.setText(thirdCategories[position].name);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewSubcategory, mContext.getResources().getString(R.string.base_url) + "images/category/third_category/" + thirdCategories[position].image, 100);
        holder.imgViewSubcategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSubcategoryClicked(thirdCategories[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return thirdCategories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_subcategory)
        TextView txtViewSubcategory;
        @BindView(R.id.img_view_subcategory)
        ImageView imgViewSubcategory;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
