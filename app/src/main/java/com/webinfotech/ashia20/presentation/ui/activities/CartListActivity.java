package com.webinfotech.ashia20.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.executors.impl.ThreadExecutor;
import com.webinfotech.ashia20.domain.models.Coupon;
import com.webinfotech.ashia20.presentation.presenters.CartDetailsPresenter;
import com.webinfotech.ashia20.presentation.presenters.impl.CartDetailsPresenterImpl;
import com.webinfotech.ashia20.presentation.ui.adapters.CartListAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.CouponCodeAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.QuantityAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.SizeAdapter;
import com.webinfotech.ashia20.presentation.ui.dialogs.CouponCodeDialog;
import com.webinfotech.ashia20.threading.MainThreadImpl;

public class CartListActivity extends AppCompatActivity implements CartDetailsPresenter.View, CouponCodeDialog.Callback {

    @BindView(R.id.recycler_view_cart_list)
    RecyclerView recyclerViewCartList;
    @BindView(R.id.txt_view_total_discount)
    TextView txtViewTotalDiscount;
    @BindView(R.id.txt_view_total_mrp)
    TextView txtViewTotalMrp;
    @BindView(R.id.txt_view_total_price)
    TextView txtViewTotalPrice;
    @BindView(R.id.txt_view_total_main)
    TextView txtViewTotalMain;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.layout_coupon_discount)
    View layoutCouponDiscount;
    @BindView(R.id.txt_view_coupon_discount)
    TextView txtViewCouponDiscount;
    @BindView(R.id.txt_view_delivery_charges)
    TextView txtViewDeliveryCharges;
    BottomSheetDialog sizeFilterBottomSheetDialog;
    BottomSheetDialog qtyFilterBottomSheetDialog;
    RecyclerView sizeFilterRecyclerview;
    RecyclerView qtyFilterRecyclerview;
    SizeAdapter sizeAdapter;
    ProgressDialog progressDialog;
    CartDetailsPresenterImpl mPresenter;
    CouponCodeDialog couponCodeDialog;
    double totalPrice;
    double totalMrp;
    String couponCode = null;
    int couponCodeDiscount = 0;
    double deliveryCharges = 0;
    int couponId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cart");
        ButterKnife.bind(this);
        setUpCouponCodeDialog();
        setSizeFilterBottomSheetDialog();
        setQtyFilterBottomSheetDialog();
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchCartDetails();
        mPresenter.fetchCoupons();
    }

    private void initialisePresenter() {
        mPresenter = new CartDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setUpCouponCodeDialog() {
        couponCodeDialog = new CouponCodeDialog(this, this, this);
        couponCodeDialog.setUpDialog();
    }

    private void setSizeFilterBottomSheetDialog() {
        if (sizeFilterBottomSheetDialog == null) {
            sizeFilterBottomSheetDialog = new BottomSheetDialog(this);
            View view = LayoutInflater.from(this).inflate(R.layout.bottom_sheet_size_filter, null);
            sizeFilterRecyclerview = (RecyclerView) view.findViewById(R.id.recycler_view_size_filter);
            View btnSubmit = (View) view.findViewById(R.id.btn_size_filter_done);
            ImageView imgViewCancel = (ImageView) view.findViewById(R.id.img_view_size_cancel);
            imgViewCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sizeFilterBottomSheetDialog.dismiss();
                }
            });
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPresenter.updateCart();
                    sizeFilterBottomSheetDialog.dismiss();
                }
            });
            sizeFilterBottomSheetDialog.setContentView(view);
        }
    }

    private void setQtyFilterBottomSheetDialog() {
        if (qtyFilterBottomSheetDialog == null) {
            qtyFilterBottomSheetDialog = new BottomSheetDialog(this);
            View view = LayoutInflater.from(this).inflate(R.layout.bottom_sheet_quantity_layout, null);
            qtyFilterRecyclerview = (RecyclerView) view.findViewById(R.id.recycler_view_quantity);
            View btnSubmit = (View) view.findViewById(R.id.btn_qty_filter_done);
            ImageView imgViewCancel = (ImageView) view.findViewById(R.id.img_view_qty_cancel);
            imgViewCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    qtyFilterBottomSheetDialog.dismiss();
                }
            });
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPresenter.updateCart();
                    qtyFilterBottomSheetDialog.dismiss();
                }
            });
            qtyFilterBottomSheetDialog.setContentView(view);
        }
    }

    @OnClick(R.id.btn_place_order) void onPlaceOrderClicked() {
        Intent intent = new Intent(this, SelectAddressActivity.class);
        intent.putExtra("totalMrp", totalMrp);
        intent.putExtra("totalPrice", totalPrice);
        intent.putExtra("deliveryCharges", deliveryCharges);
        intent.putExtra("couponCode", couponCode);
        intent.putExtra("couponDiscount", couponCodeDiscount);
        intent.putExtra("couponId", couponId);
        startActivity(intent);
    }

    @OnClick(R.id.layout_apply_coupon) void onCouponApplyClicked() {
        couponCodeDialog.showDialog();
    }

    @Override
    public void loadData(CartListAdapter cartListAdapter, double mrp, double price) {
        recyclerViewCartList.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewCartList.setAdapter(cartListAdapter);
        txtViewTotalMrp.setText("Rs. " + mrp);
        txtViewTotalPrice.setText("Rs. " + price);
        txtViewTotalDiscount.setText("-Rs. " + (mrp - price));
        txtViewTotalMain.setText("Rs. " + price);
        totalPrice = price;
        totalMrp = mrp;
        mainLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void loadSizeAdapter(SizeAdapter sizeAdapter) {
        sizeFilterRecyclerview.setAdapter(sizeAdapter);
        sizeFilterRecyclerview.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        sizeFilterBottomSheetDialog.show();
    }

    @Override
    public void loadQuantityAdapter(QuantityAdapter quantityAdapter) {
        qtyFilterRecyclerview.setAdapter(quantityAdapter);
        qtyFilterRecyclerview.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        qtyFilterBottomSheetDialog.show();
    }

    @Override
    public void loadCouponData(Coupon coupon) {
        couponCodeDialog.setCouponData(coupon);
    }

    @Override
    public void loadDiscount(int discount, int couponId) {
        this.couponCodeDiscount = discount;
        this.couponId = couponId;
        double newTotal = totalPrice - totalPrice*discount/100;
        double totalDiscount = totalPrice*discount/100;
        if (deliveryCharges > 0) {
            newTotal = newTotal + deliveryCharges;
            txtViewTotalPrice.setText("Rs. " + newTotal);
            txtViewTotalMain.setText("Rs. " + newTotal);
            txtViewCouponDiscount.setText("-Rs. " + totalDiscount);
            layoutCouponDiscount.setVisibility(View.VISIBLE);
        } else {
            txtViewTotalPrice.setText("Rs. " + newTotal);
            txtViewTotalMain.setText("Rs. " + newTotal);
            txtViewCouponDiscount.setText("-Rs. " + totalDiscount);
            layoutCouponDiscount.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void loadShippingCharges(double minAmount, double shipingCharges) {
        if (totalPrice < minAmount) {
            double newTotal = totalPrice + shipingCharges;
            txtViewDeliveryCharges.setText("+Rs. " + shipingCharges);
            txtViewDeliveryCharges.setTextColor(getResources().getColor(R.color.md_red_400));
            txtViewTotalPrice.setText("Rs. " + newTotal);
            txtViewTotalMain.setText("Rs. " + newTotal);
            deliveryCharges = shipingCharges;
            if (couponCodeDiscount > 0) {
                Log.e("LogMsg", "Coupon Code Discount: " + couponCodeDiscount);
                newTotal = newTotal - totalPrice*couponCodeDiscount/100;
                double totalDiscount = totalPrice*couponCodeDiscount/100;
                txtViewTotalPrice.setText("Rs. " + newTotal);
                txtViewTotalMain.setText("Rs. " + newTotal);
                txtViewCouponDiscount.setText("-Rs. " + totalDiscount);
                layoutCouponDiscount.setVisibility(View.VISIBLE);
            }
        } else {
            deliveryCharges = 0;
            txtViewDeliveryCharges.setText("FREE");
            txtViewDeliveryCharges.setTextColor(getResources().getColor(R.color.md_teal_A400));
            txtViewTotalPrice.setText("Rs. " + totalPrice);
            txtViewTotalMain.setText("Rs. " + totalPrice);
            if (couponCodeDiscount > 0) {
                Log.e("LogMsg", "Coupon Code Discount: " + couponCodeDiscount);
                double newTotal = totalPrice - totalPrice*couponCodeDiscount/100;
                double totalDiscount = totalPrice*couponCodeDiscount/100;
                txtViewTotalPrice.setText("Rs. " + newTotal);
                txtViewTotalMain.setText("Rs. " + newTotal);
                txtViewCouponDiscount.setText("-Rs. " + totalDiscount);
                layoutCouponDiscount.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void hideViews() {
        mainLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void onApplyCouponClicked(String coupon) {
        couponCodeDialog.hideDialog();
        mPresenter.applyCoupon(coupon);
        this.couponCode = coupon;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
