package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.FilterValue;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterValueAdapter extends RecyclerView.Adapter<FilterValueAdapter.ViewHolder> {

    public interface Callback {
        void onFilterValueClicked(int filterValueId, int filterNameId);
    }

    Context mContext;
    FilterValue[] filterValues;
    Callback mCallback;

    public FilterValueAdapter(Context mContext, FilterValue[] filterValues, Callback mCallback) {
        this.mContext = mContext;
        this.filterValues = filterValues;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_filter_value, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewFilterValue.setText(filterValues[position].name);
        if (filterValues[position].isSelected) {
            holder.imgViewCheckRed.setVisibility(View.VISIBLE);
            holder.imgViewCheckGrey.setVisibility(View.GONE);
        } else {
            holder.imgViewCheckRed.setVisibility(View.GONE);
            holder.imgViewCheckGrey.setVisibility(View.VISIBLE);
        }

        if (filterValues[position].code == null) {
            holder.viewColorBg.setVisibility(View.GONE);
        } else {
            holder.viewColorBg.setVisibility(View.VISIBLE);
            holder.viewColorBg.setBackgroundColor(Color.parseColor(filterValues[position].code));
        }

        holder.txtViewProductCount.setText(Integer.toString(filterValues[position].productCount));

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onFilterValueClicked(filterValues[position].id, filterValues[position].filterNameId);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filterValues.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_filter_value)
        TextView txtViewFilterValue;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.view_color_bg)
        View viewColorBg;
        @BindView(R.id.img_view_check_grey)
        ImageView imgViewCheckGrey;
        @BindView(R.id.img_view_check_red)
        ImageView imgViewCheckRed;
        @BindView(R.id.txt_view_product_count)
        TextView txtViewProductCount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataset(FilterValue[] filterValues) {
        this.filterValues = filterValues;
        notifyDataSetChanged();
    }

}
