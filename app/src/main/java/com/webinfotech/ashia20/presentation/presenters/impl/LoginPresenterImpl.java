package com.webinfotech.ashia20.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.ashia20.AndroidApplication;
import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.CheckLoginInteractor;
import com.webinfotech.ashia20.domain.interactors.impl.CheckLoginInteractorImpl;
import com.webinfotech.ashia20.domain.models.UserInfo;
import com.webinfotech.ashia20.presentation.presenters.LoginPresenter;
import com.webinfotech.ashia20.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, CheckLoginInteractor.Callback {

    Context mContext;
    LoginPresenter.View mView;
    AndroidApplication androidApplication;
    CheckLoginInteractorImpl checkLoginInteractor;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkLogin(String email, String password) {
        checkLoginInteractor = new CheckLoginInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, email, password);
        checkLoginInteractor.execute();
    }

    @Override
    public void onLoginSuccess(UserInfo userInfo) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        mView.hideLoader();
        Toasty.success(mContext, "Login Successful").show();
        mView.goToMainActivity();
    }

    @Override
    public void onLoginFail(String errorMsg) {
        Toasty.warning(mContext, errorMsg).show();
        mView.hideLoader();
    }
}
