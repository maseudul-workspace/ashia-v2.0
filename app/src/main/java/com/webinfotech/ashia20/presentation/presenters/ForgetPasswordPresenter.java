package com.webinfotech.ashia20.presentation.presenters;

public interface ForgetPasswordPresenter {
    void forgetPassword(String mobile, String password);
    interface View {
        void showLoader();
        void hideLoader();
    }
}
