package com.webinfotech.ashia20.presentation.presenters;

import com.webinfotech.ashia20.presentation.ui.adapters.FilterNameAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.ProductListVerticalAdapter;

import java.util.ArrayList;

public interface ProductListPresenter {
    void fetchProductListWithFilter(int categoryId, int type);
    void fetchProductListWithoutFilter(int categoryId,
                                       int page,
                                       int type,
                                       ArrayList<Integer> brands,
                                       ArrayList<Integer> colors,
                                       ArrayList<Integer> sizes,
                                       int priceFrom,
                                       int priceTo,
                                       int sort,
                                       String refresh
                                       );
    void fetchWishlist();
    interface View {
        void loadProductsWithFilter(ProductListVerticalAdapter adapter, int totalPage);
        void onProductClicked(int productId);
        void showLoginBottomSheet();
        void showLoader();
        void hideLoader();
    }
}
