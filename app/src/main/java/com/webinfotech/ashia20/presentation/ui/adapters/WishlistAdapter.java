package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Wishlist;
import com.webinfotech.ashia20.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WishlistAdapter extends RecyclerView.Adapter<WishlistAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int productId);
        void removeFromWishlist(int productId, int position);
    }

    Context mContext;
    Wishlist[] wishlists;
    Callback mCallback;

    public WishlistAdapter(Context mContext, Wishlist[] wishlists, Callback mCallback) {
        this.mContext = mContext;
        this.wishlists = wishlists;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_product_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewProductName.setText(wishlists[position].product.name);
        holder.txtViewBrand.setText(wishlists[position].product.brandName);
        holder.txtViewPrice.setText("₹ " + wishlists[position].product.minPrice);
        int productDiscount = (int) (100 -  wishlists[position].product.minPrice/wishlists[position].product.mrp * 100);
        holder.txtViewDiscount.setText(productDiscount + "% off");
        holder.txtViewMrp.setText("₹ " + wishlists[position].product.mrp);
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "images/products/" + wishlists[position].product.mainImage);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(wishlists[position].product.id);
            }
        });
        holder.imgViewBookmarkRed.setVisibility(View.VISIBLE);
        holder.imgViewBookmarkRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.removeFromWishlist(wishlists[position].product.id, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return wishlists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.txt_view_discount)
        TextView txtViewDiscount;
        @BindView(R.id.txt_view_brand)
        TextView txtViewBrand;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.img_view_bookmark_grey)
        ImageView imgViewBookmarkGrey;
        @BindView(R.id.img_view_bookmark_red)
        ImageView imgViewBookmarkRed;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
