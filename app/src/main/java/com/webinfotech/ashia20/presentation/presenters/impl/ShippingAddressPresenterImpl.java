package com.webinfotech.ashia20.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.ashia20.AndroidApplication;
import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.DeleteShippingAddressInteractor;
import com.webinfotech.ashia20.domain.interactors.FetchShippingAddressInteractor;
import com.webinfotech.ashia20.domain.interactors.impl.DeleteShippingAddressInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import com.webinfotech.ashia20.domain.models.ShippingAddress;
import com.webinfotech.ashia20.domain.models.UserInfo;
import com.webinfotech.ashia20.presentation.presenters.ShippingAddressPresenter;
import com.webinfotech.ashia20.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.ashia20.presentation.ui.adapters.ShippingAddressAdapter;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ShippingAddressPresenterImpl extends AbstractPresenter implements  ShippingAddressPresenter,
                                                                                FetchShippingAddressInteractor.Callback,
                                                                                ShippingAddressAdapter.Callback,
                                                                                DeleteShippingAddressInteractor.Callback
{

    Context mContext;
    ShippingAddressPresenter.View mView;
    FetchShippingAddressInteractorImpl fetchShippingAddressInteractor;
    DeleteShippingAddressInteractorImpl deleteShippingAddressInteractor;
    ShippingAddress[] shippingAddresses;
    ShippingAddressAdapter shippingAddressAdapter;

    public ShippingAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchShippingAddress() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            fetchShippingAddressInteractor = new FetchShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchShippingAddressInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingAddressListSucces(ShippingAddress[] shippingAddresses) {
        if (shippingAddresses.length > 0) {
            this.shippingAddresses = shippingAddresses;
            this.shippingAddresses[0].isSelected = true;
            mView.onAddressSelected(shippingAddresses[0].id);
            shippingAddressAdapter = new ShippingAddressAdapter(mContext, this.shippingAddresses, this);
            mView.loadAdapter(shippingAddressAdapter);
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingAddressListFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.onAddressFetchFailed();
    }

    @Override
    public void onEditClicked(int id) {
        mView.goToAddressDetails(id);
    }

    @Override
    public void onDeleteClicked(int id) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            deleteShippingAddressInteractor = new DeleteShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, id, user.apiToken);
            deleteShippingAddressInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onAddressSelected(int id) {
       for (int i = 0; i < this.shippingAddresses.length; i++) {
           if (this.shippingAddresses[i].id == id) {
               mView.onAddressSelected(shippingAddresses[i].id);
               this.shippingAddresses[i].isSelected = true;
           } else {
               this.shippingAddresses[i].isSelected = false;
           }
       }
       shippingAddressAdapter.updateDataSet(this.shippingAddresses);
    }

    @Override
    public void onAddressDeleteSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Address Deleted Successfully").show();
        fetchShippingAddress();
    }

    @Override
    public void onAddressDeleteFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
