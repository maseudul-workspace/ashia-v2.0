package com.webinfotech.ashia20.presentation.presenters;

import com.webinfotech.ashia20.presentation.ui.adapters.ShippingAddressAdapter;

public interface ShippingAddressPresenter {
    void fetchShippingAddress();
    interface View {
        void loadAdapter(ShippingAddressAdapter adapter);
        void goToAddressDetails(int addressId);
        void onAddressSelected(int addressId);
        void onAddressFetchFailed();
        void showLoader();
        void hideLoader();
    }
}
