package com.webinfotech.ashia20.presentation.presenters;

import com.webinfotech.ashia20.domain.models.UserInfo;

public interface UserProfilePresenter {
    void fetchUserProfile();
    void updateProfile(String name,
                       String email,
                       String mobile,
                       String DOB,
                       String gender,
                       String city,
                       String state,
                       String pin,
                       String address);
    interface View {
        void showLoader();
        void hideLoader();
        void loadUserProfileData(UserInfo userInfo);
    }
}
