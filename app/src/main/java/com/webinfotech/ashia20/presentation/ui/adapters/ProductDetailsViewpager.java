package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Image;
import com.webinfotech.ashia20.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class ProductDetailsViewpager extends PagerAdapter {

    public interface Callback {
        void onImageClicked(int position);
    }

    Context mContext;
    Image[] images;
    Callback mCallback;

    public ProductDetailsViewpager(Context mContext, Image[] images, Callback mCallback) {
        this.mContext = mContext;
        this.images = images;
        this.mCallback = mCallback;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.product_details_viewpager, container, false);
        ImageView imageView = (ImageView) layout.findViewById(R.id.img_view_viewpager);
        GlideHelper.setImageView(mContext, imageView, mContext.getResources().getString(R.string.base_url) + "images/products/" + images[position].image);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onImageClicked(position);
            }
        });
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
