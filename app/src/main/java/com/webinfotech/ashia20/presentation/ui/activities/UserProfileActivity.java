package com.webinfotech.ashia20.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.executors.impl.ThreadExecutor;
import com.webinfotech.ashia20.domain.models.UserInfo;
import com.webinfotech.ashia20.presentation.presenters.UserProfilePresenter;
import com.webinfotech.ashia20.presentation.presenters.impl.UserProfilePresenterImpl;
import com.webinfotech.ashia20.threading.MainThreadImpl;

import java.util.Calendar;

public class UserProfileActivity extends AppCompatActivity implements UserProfilePresenter.View {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_pincode)
    EditText editTextPin;
    @BindView(R.id.txt_view_dob)
    TextView txtViewDOB;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    String dob;
    @BindView(R.id.radio_group_gender)
    RadioGroup radioGroupGender;
    @BindView(R.id.radio_btn_male)
    RadioButton radioBtnMale;
    @BindView(R.id.radio_btn_female)
    RadioButton radioBtnFemale;
    String gender;
    ProgressDialog progressDialog;
    UserProfilePresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        getSupportActionBar().setTitle("User Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setRadioGroupGender();
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchUserProfile();
    }

    private void initialisePresenter() {
        mPresenter = new UserProfilePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setRadioGroupGender() {
        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb=(RadioButton)findViewById(checkedId);
                gender = rb.getText().toString();
                if (gender.equals("Male")) {
                    gender = "M";
                } else {
                    gender = "F";
                }
            }
        });
    }

    @OnClick(R.id.dob_linear_layout) void onDOBLayoutClicked() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        txtViewDOB.setText(dob);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loadUserProfileData(UserInfo userInfo) {
        editTextName.setText(userInfo.name);
        editTextAddress.setText(userInfo.address);
        editTextCity.setText(userInfo.city);
        editTextEmail.setText(userInfo.email);
        editTextPhone.setText(userInfo.mobile);
        editTextPin.setText(userInfo.pin);
        editTextState.setText(userInfo.state);
        txtViewDOB.setText(userInfo.dob);
        if (userInfo.gender.equals("M")) {
            radioBtnMale.setChecked(true);
        } else {
            radioBtnFemale.setChecked(true);
        }
        gender = userInfo.gender;
        dob = userInfo.dob;
    }

    @OnClick(R.id.btn_update_profile) void onUpdateProfileClicked() {
        if (editTextName.getText().toString().trim().isEmpty() ||
                editTextEmail.getText().toString().isEmpty() ||
                editTextPhone.getText().toString().trim().isEmpty() ) {
            Toasty.warning(this, "Please fill all the fields").show();
        } else {
            mPresenter.updateProfile(
                    editTextName.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextPhone.getText().toString(),dob,
                    gender,
                    editTextCity.getText().toString(),
                    editTextState.getText().toString(),
                    editTextPin.getText().toString(),
                    editTextAddress.getText().toString()
            );
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}