package com.webinfotech.ashia20.presentation.presenters;

import com.webinfotech.ashia20.domain.models.Coupon;
import com.webinfotech.ashia20.domain.models.testing.Quantity;
import com.webinfotech.ashia20.presentation.ui.adapters.CartListAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.CouponCodeAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.QuantityAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.SizeAdapter;

public interface CartDetailsPresenter {
    void fetchCartDetails();
    void updateCart();
    void fetchCoupons();
    void applyCoupon(String coupon);
    void fetchShippingCharges();
    interface View {
        void loadData(CartListAdapter cartListAdapter, double mrp, double price);
        void loadSizeAdapter(SizeAdapter sizeAdapter);
        void loadQuantityAdapter(QuantityAdapter quantityAdapter);
        void loadCouponData(Coupon coupon);
        void loadDiscount(int discount, int couponId);
        void loadShippingCharges(double minAmount, double shipingCharges);
        void hideViews();
        void showLoader();
        void hideLoader();
    }
}
