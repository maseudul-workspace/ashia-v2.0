package com.webinfotech.ashia20.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.ashia20.AndroidApplication;
import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.PlaceOrderInteractor;
import com.webinfotech.ashia20.domain.interactors.VerifyPaymentInteractor;
import com.webinfotech.ashia20.domain.interactors.impl.PlaceOrderInteractorImpl;
import com.webinfotech.ashia20.domain.interactors.impl.VerifyPaymentInteractorImpl;
import com.webinfotech.ashia20.domain.models.PaymentDataMain;
import com.webinfotech.ashia20.domain.models.UserInfo;
import com.webinfotech.ashia20.presentation.presenters.PaymentPresenter;
import com.webinfotech.ashia20.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class PaymentPresenterImpl extends AbstractPresenter implements PaymentPresenter, PlaceOrderInteractor.Callback, VerifyPaymentInteractor.Callback {

    Context mContext;
    PaymentPresenter.View mView;

    public PaymentPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void placeOrder(String couponId, int paymentType, int shippingAddressId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            PlaceOrderInteractorImpl placeOrderInteractor = new PlaceOrderInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, couponId, paymentType, shippingAddressId);
            placeOrderInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void verifyPayment(String razorpayOrderId, String razorpayPaymentId, String razorpaySignature, int orderId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            VerifyPaymentInteractorImpl verifyPaymentInteractor = new VerifyPaymentInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, razorpayOrderId, razorpayPaymentId, razorpaySignature, orderId);
            verifyPaymentInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onPlaceOrderSuccess(PaymentDataMain paymentDataMain) {
        if (paymentDataMain.paymentStatus == 1) {
            mView.hideLoader();
            Toasty.success(mContext, "Order Placed Successfully").show();
            mView.goToOrderHistory();
        } else {
            mView.hideLoader();
            mView.initiatePayment(paymentDataMain.paymentData, paymentDataMain.orderId);
        }
    }

    @Override
    public void onPlaceOrderFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onVerifyPaymentSuccess() {
        mView.hideLoader();
        mView.onPaymentVerificationSuccess();
    }

    @Override
    public void onVerifyPaymentFail(String errorMsg) {
        mView.hideLoader();
        mView.onPaymentVerificationSuccess();
    }
}
