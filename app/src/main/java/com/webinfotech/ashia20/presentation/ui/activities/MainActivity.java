package com.webinfotech.ashia20.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.executors.impl.ThreadExecutor;
import com.webinfotech.ashia20.domain.models.testing.Brand;
import com.webinfotech.ashia20.domain.models.testing.Category;
import com.webinfotech.ashia20.domain.models.testing.Image;
import com.webinfotech.ashia20.presentation.presenters.MainPresenter;
import com.webinfotech.ashia20.presentation.presenters.impl.MainPresenterImpl;
import com.webinfotech.ashia20.presentation.ui.adapters.BrandsMainAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.CategoriesAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.CategoryMainAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.MainViewpagerAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.SubcategoryMainAdapter;
import com.webinfotech.ashia20.threading.MainThreadImpl;
import com.webinfotech.ashia20.util.GlideHelper;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends BaseActivity implements MainPresenter.View {

    @BindView(R.id.recycler_view_categories)
    RecyclerView recyclerViewCategories;
    @BindView(R.id.img_view_first_poster)
    ImageView imgViewFirstPoster;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.dot_layout)
    LinearLayout dotsLayout;
    @BindView(R.id.recycler_view_brands_main)
    RecyclerView recyclerViewBrandsMain;
    @BindView(R.id.recycler_view_category_main)
    RecyclerView recyclerViewCategoryMain;
    int currentPage = 0;
    MainPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        ButterKnife.bind(this);
        initialisePresenter();
        mPresenter.fetchMainData();
        mPresenter.fetchWishlist();
    }

    private void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadData(CategoriesAdapter categoriesAdapter, String bannerImage, int imageCount, MainViewpagerAdapter mainViewpagerAdapter, BrandsMainAdapter brandsMainAdapter, CategoryMainAdapter categoryMainAdapter) {
        recyclerViewCategories.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewCategories.setAdapter(categoriesAdapter);

        recyclerViewBrandsMain.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerViewBrandsMain.setAdapter(brandsMainAdapter);

        recyclerViewCategoryMain.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewCategoryMain.setAdapter(categoryMainAdapter);

        viewPager.setAdapter(mainViewpagerAdapter);
        prepareDotsIndicator(0, imageCount);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
            prepareDotsIndicator(position, imageCount);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        GlideHelper.setImageView(this, imgViewFirstPoster, getResources().getString(R.string.base_url) + "images/slider/banner/" + bannerImage);

    }

    @Override
    public void goToThirdCategory(int subcategoryId) {
        Intent intent = new Intent(this, ThirdCategoryActivity.class);
        intent.putExtra("subcategoryId", subcategoryId);
        startActivity(intent);
    }

    @Override
    public void goToProductList(int subcategoryId) {

    }

    private void prepareDotsIndicator(int sliderPosition, int length) {
        if(dotsLayout.getChildCount() > 0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[length];
        for(int i = 0; i < length; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(7, 0, 7, 0);
            dotsLayout.addView(dots[i], layoutParams);

        }
    }

    @Override
    public void onCategoryClicked(int categoryId) {
        Intent intent = new Intent(this, SubcategoryActivity.class);
        intent.putExtra("categoryId", categoryId);
        startActivity(intent);
    }
}
