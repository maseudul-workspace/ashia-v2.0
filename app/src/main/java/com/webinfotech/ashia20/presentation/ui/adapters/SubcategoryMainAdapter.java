package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Subcategory;
import com.webinfotech.ashia20.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SubcategoryMainAdapter extends RecyclerView.Adapter<SubcategoryMainAdapter.ViewHolder> {

    public interface Callback {
        void goToThirdCategory(int subcategoryId);
        void goToProductList(int subcategoryid);
    }

    Context mContext;
    Subcategory[] subcategories;
    Callback mCallback;

    public SubcategoryMainAdapter(Context mContext, Subcategory[] subcategories, Callback mCallback) {
        this.mContext = mContext;
        this.subcategories = subcategories;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_main_subcategory, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSubcategoryMain.setText(subcategories[position].name);
        GlideHelper.setImageView(mContext, holder.imgViewSubcategoryMain, mContext.getResources().getString(R.string.base_url) + "images/category/sub_category/" + subcategories[position].image);
        holder.imgViewSubcategoryMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (subcategories[position].isSubCategory == 1) {
                    mCallback.goToProductList(subcategories[position].id);
                } else {
                    mCallback.goToThirdCategory(subcategories[position].id);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return subcategories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_subcategory_main)
        TextView txtViewSubcategoryMain;
        @BindView(R.id.img_view_subcategory_main)
        ImageView imgViewSubcategoryMain;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
