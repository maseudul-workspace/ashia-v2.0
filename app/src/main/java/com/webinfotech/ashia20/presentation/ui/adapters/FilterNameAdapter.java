package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.FilterName;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterNameAdapter extends RecyclerView.Adapter<FilterNameAdapter.ViewHolder> {

    public interface Callback {
        void onFilterNameClicked(int filterNameId);
    }

    Context mContext;
    FilterName[] filterNames;
    Callback mCallback;

    public FilterNameAdapter(Context mContext, FilterName[] filterNames, Callback mCallback) {
        this.mContext = mContext;
        this.filterNames = filterNames;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_filter_name, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewFilterName.setText(filterNames[position].name);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onFilterNameClicked(filterNames[position].id);
            }
        });
        if (filterNames[position].isSelected) {
            holder.txtViewFilterName.setBackgroundColor(mContext.getResources().getColor(R.color.md_white_1000));
        } else {
            holder.txtViewFilterName.setBackgroundColor(mContext.getResources().getColor(R.color.md_grey_200));
        }
    }

    @Override
    public int getItemCount() {
        return filterNames.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_filter_name)
        TextView txtViewFilterName;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataset(FilterName[] filterNames) {
        this.filterNames = filterNames;
        notifyDataSetChanged();
    }

}
