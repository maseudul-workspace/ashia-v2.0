package com.webinfotech.ashia20.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.executors.impl.ThreadExecutor;
import com.webinfotech.ashia20.domain.models.testing.Product;
import com.webinfotech.ashia20.presentation.presenters.ChangePasswordPresenter;
import com.webinfotech.ashia20.presentation.presenters.impl.ChangePasswordPresneterImpl;
import com.webinfotech.ashia20.threading.MainThreadImpl;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordPresenter.View {

    @BindView(R.id.txt_input_old_password)
    TextInputLayout txtInputOldPassword;
    @BindView(R.id.txt_input_new_password)
    TextInputLayout txtInputNewPassword;
    @BindView(R.id.txt_input_confirm_password)
    TextInputLayout txtInputConfirmPassword;
    @BindView(R.id.edit_text_old_password)
    EditText editTextOldPassword;
    @BindView(R.id.edit_text_new_password)
    EditText editTextNewPassword;
    @BindView(R.id.edit_text_confirm_password)
    EditText editTextConfirmPassword;
    ChangePasswordPresneterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Change Password");
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new ChangePasswordPresneterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_change_password) void onChangePasswordClicked() {
        txtInputConfirmPassword.setError("");
        txtInputNewPassword.setError("");
        txtInputOldPassword.setError("");
        if (editTextOldPassword.getText().toString().trim().isEmpty()) {
            txtInputOldPassword.setError("Please Enter Your Current Password");
        } else if (editTextNewPassword.getText().toString().trim().isEmpty()) {
            txtInputNewPassword.setError("Please Enter New Password");
        } else if (editTextConfirmPassword.getText().toString().trim().isEmpty()) {
            txtInputConfirmPassword.setError("Please Confirm Password");
        } else if (!editTextNewPassword.getText().toString().trim().equals(editTextConfirmPassword.getText().toString().trim())) {
            Toasty.warning(this, "Password Mismatch").show();
        } else {
            mPresenter.changePassword(editTextOldPassword.getText().toString(), editTextNewPassword.getText().toString());
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}