package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.Orders;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> implements OrderProductsAdapter.Callback {

    @Override
    public void onCancelClicked(int itemOrderId) {
        mCallback.onCancelClicked(itemOrderId);
    }

    @Override
    public void onReturnClicked(int itemOrderId) {
        mCallback.onReturnClicked(itemOrderId);
    }

    public interface Callback {
        void onCancelClicked(int itemOrderId);
        void onReturnClicked(int itemOrderId);
    }

    Context mContext;
    Orders[] orders;
    Callback mCallback;

    public OrdersAdapter(Context mContext, Orders[] orders, Callback mCallback) {
        this.mContext = mContext;
        this.orders = orders;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_order_history, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txtViewOrderNo.setText("ORDER NO " + orders[position].orderId);
        holder.txtViewTotalAmount.setText("Rs. " + orders[position].totalAmount);

        if (orders[position].shippingCharge < 1) {
            holder.txtViewDeliveryCharges.setText("Free");
        } else {
            holder.txtViewDeliveryCharges.setText("Rs. " + orders[position].shippingCharge);
        }

        if (orders[position].paymentStatus == 1) {
            holder.txtViewPaymentStatus.setText("COD");
        } else if (orders[position].paymentStatus == 2) {
            holder.txtViewPaymentStatus.setText("Paid");
        } else {
            holder.txtViewPaymentStatus.setText("Failed");
        }

        if (orders[position].discount < 1) {
            holder.layoutCouponDiscount.setVisibility(View.GONE);
            holder.txtViewCouponDiscount.setText("Rs. 0");
        } else {
            holder.layoutCouponDiscount.setVisibility(View.VISIBLE);
            holder.txtViewCouponDiscount.setText("Rs. " + orders[position].discount);
        }

        if (orders[position].paymentType == 1) {
            holder.txtViewPaymentMethod.setText("COD");
        } else {
            holder.txtViewPaymentMethod.setText("Online");
        }

        OrderProductsAdapter adapter = new OrderProductsAdapter(mContext, orders[position].orderProducts, this);
        holder.recyclerViewOrderProducts.setAdapter(adapter);
        holder.recyclerViewOrderProducts.setLayoutManager(new LinearLayoutManager(mContext));

    }

    @Override
    public int getItemCount() {
        return orders.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_order_no)
        TextView txtViewOrderNo;
        @BindView(R.id.txt_view_order_date)
        TextView txtViewOrderDate;
        @BindView(R.id.txt_view_total_amount)
        TextView txtViewTotalAmount;
        @BindView(R.id.txt_view_delivery_charges)
        TextView txtViewDeliveryCharges;
        @BindView(R.id.txt_view_payment_method)
        TextView txtViewPaymentMethod;
        @BindView(R.id.txt_view_payment_status)
        TextView txtViewPaymentStatus;
        @BindView(R.id.recycler_view_order_products)
        RecyclerView recyclerViewOrderProducts;
        @BindView(R.id.layout_coupon_discount)
        View layoutCouponDiscount;
        @BindView(R.id.txt_view_coupon_discount)
        TextView txtViewCouponDiscount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
