package com.webinfotech.ashia20.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.executors.impl.ThreadExecutor;
import com.webinfotech.ashia20.presentation.presenters.ThirdCategoryPresenter;
import com.webinfotech.ashia20.presentation.presenters.impl.ThirdCategoryPresenterImpl;
import com.webinfotech.ashia20.presentation.ui.adapters.ThirdCategoryAdapter;
import com.webinfotech.ashia20.presentation.ui.adapters.ThirdCategoryViewpagerAdapter;
import com.webinfotech.ashia20.threading.MainThreadImpl;
import com.webinfotech.ashia20.util.GlideHelper;

public class ThirdCategoryActivity extends AppCompatActivity implements ThirdCategoryPresenter.View {

    @BindView(R.id.recycler_view_third_subcategory)
    RecyclerView recyclerViewThirdSubcategory;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.dot_layout)
    LinearLayout dotsLayout;
    ProgressDialog progressDialog;
    ThirdCategoryPresenterImpl mPresenter;
    int subcategoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third_category);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        subcategoryId = getIntent().getIntExtra("subcategoryId", 0);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchThirdCategory(subcategoryId);
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new ThirdCategoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void prepareDotsIndicator(int sliderPosition, int length) {
        if(dotsLayout.getChildCount() > 0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[length];
        for(int i = 0; i < length; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(7, 0, 7, 0);
            dotsLayout.addView(dots[i], layoutParams);

        }
    }

    @Override
    public void loadAdapter(ThirdCategoryAdapter adapter, ThirdCategoryViewpagerAdapter viewpagerAdapter, int imageCount) {

        recyclerViewThirdSubcategory.setAdapter(adapter);
        recyclerViewThirdSubcategory.setLayoutManager(new GridLayoutManager(this, 3));

        prepareDotsIndicator(0, imageCount);
        viewPager.setAdapter(viewpagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareDotsIndicator(position, imageCount);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}