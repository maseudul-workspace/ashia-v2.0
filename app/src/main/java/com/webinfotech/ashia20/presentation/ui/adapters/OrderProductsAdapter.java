package com.webinfotech.ashia20.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.transition.Hold;
import com.webinfotech.ashia20.R;
import com.webinfotech.ashia20.domain.models.OrderProducts;
import com.webinfotech.ashia20.domain.models.Orders;
import com.webinfotech.ashia20.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderProductsAdapter extends RecyclerView.Adapter<OrderProductsAdapter.ViewHolder> {

    public interface Callback {
        void onCancelClicked(int orderId);
        void onReturnClicked(int orderId);
    }

    Context mContext;
    OrderProducts[] orderProducts;
    Callback mCallback;

    public OrderProductsAdapter(Context mContext, OrderProducts[] orderProducts, Callback mCallback) {
        this.mContext = mContext;
        this.orderProducts = orderProducts;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_order_products, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewProductName.setText(orderProducts[position].productName);
        holder.txtViewSizeQty.setText("Size: " + orderProducts[position].size + " | Qty: " + orderProducts[position].quantity);
        holder.txtViewPrice.setText("₹ " + orderProducts[position].price);
        holder.txtViewMrp.setText("₹ " + orderProducts[position].mrp);
        holder.txtViewSaved.setText("Saved ₹ " + (orderProducts[position].mrp - orderProducts[position].price));
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        GlideHelper.setImageView(mContext, holder.imgViewProductPoster, mContext.getResources().getString(R.string.base_url) + "images/products/" + orderProducts[position].productImage);
        holder.imgViewProductPoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        holder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirmation dialog");
                builder.setMessage("You are about to cancel a order. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton(Html.fromHtml("<font color='#00BFA5'>Yes</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onCancelClicked(orderProducts[position].orderItemId);
                    }
                });

                builder.setNegativeButton(Html.fromHtml("<font color='#FF1744'>No</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });

        holder.btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirmation dialog");
                builder.setMessage("You are about to return a order. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton(Html.fromHtml("<font color='#00BFA5'>Yes</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onReturnClicked(orderProducts[position].orderItemId);
                    }
                });

                builder.setNegativeButton(Html.fromHtml("<font color='#FF1744'>No</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });

        if (orderProducts[position].orderStatus == 1) {
            holder.viewOrderStatusRed.setVisibility(View.GONE);
            holder.viewOrderStatusGreen.setVisibility(View.VISIBLE);
            holder.txtViewOrderStatus.setVisibility(View.VISIBLE);
            holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_teal_A700));
            holder.txtViewOrderStatus.setText("Order Placed");
            holder.viewInProcessGreen.setVisibility(View.GONE);
            holder.viewInProcessGrey.setVisibility(View.VISIBLE);
            holder.viewRoundInProcessGreen.setVisibility(View.GONE);
            holder.viewRoundInProcessGrey.setVisibility(View.VISIBLE);
            holder.viewRoundShippedGreen.setVisibility(View.GONE);
            holder.viewShippedGreen.setVisibility(View.GONE);
            holder.viewShippedGrey.setVisibility(View.VISIBLE);
            holder.viewRoundShippedGrey.setVisibility(View.VISIBLE);
            holder.viewDeliveryGreen.setVisibility(View.GONE);
            holder.viewDeliveryGrey.setVisibility(View.VISIBLE);
            holder.viewRoundDeliveredGreen.setVisibility(View.GONE);
            holder.viewRoundDeliveredGrey.setVisibility(View.VISIBLE);
            holder.viewRoundDeliveredRed.setVisibility(View.GONE);
            holder.txtViewDelivered.setText("DELIVERED");
            holder.txtViewDelivered.setTextColor(mContext.getResources().getColor(R.color.md_grey_900));
            holder.btnCancel.setVisibility(View.VISIBLE);
            holder.btnReturn.setVisibility(View.GONE);
        } else if (orderProducts[position].orderStatus == 2) {
            holder.viewOrderStatusRed.setVisibility(View.GONE);
            holder.viewOrderStatusGreen.setVisibility(View.VISIBLE);
            holder.txtViewOrderStatus.setVisibility(View.VISIBLE);
            holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_teal_A700));
            holder.txtViewOrderStatus.setText("Processing");
            holder.viewInProcessGreen.setVisibility(View.VISIBLE);
            holder.viewInProcessGrey.setVisibility(View.GONE);
            holder.viewRoundInProcessGreen.setVisibility(View.VISIBLE);
            holder.viewRoundInProcessGrey.setVisibility(View.GONE);
            holder.viewRoundShippedGreen.setVisibility(View.GONE);
            holder.viewShippedGreen.setVisibility(View.GONE);
            holder.viewShippedGrey.setVisibility(View.VISIBLE);
            holder.viewRoundShippedGrey.setVisibility(View.VISIBLE);
            holder.viewDeliveryGreen.setVisibility(View.GONE);
            holder.viewDeliveryGrey.setVisibility(View.VISIBLE);
            holder.viewRoundDeliveredGreen.setVisibility(View.GONE);
            holder.viewRoundDeliveredGrey.setVisibility(View.VISIBLE);
            holder.viewRoundDeliveredRed.setVisibility(View.GONE);
            holder.txtViewDelivered.setText("DELIVERED");
            holder.txtViewDelivered.setTextColor(mContext.getResources().getColor(R.color.md_grey_900));
            holder.btnCancel.setVisibility(View.VISIBLE);
            holder.btnReturn.setVisibility(View.GONE);
        } else if (orderProducts[position].orderStatus == 3) {
            holder.viewOrderStatusRed.setVisibility(View.GONE);
            holder.viewOrderStatusGreen.setVisibility(View.VISIBLE);
            holder.txtViewOrderStatus.setVisibility(View.VISIBLE);
            holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_teal_A700));
            holder.txtViewOrderStatus.setText("Dispatched");
            holder.viewInProcessGreen.setVisibility(View.VISIBLE);
            holder.viewInProcessGrey.setVisibility(View.GONE);
            holder.viewRoundInProcessGreen.setVisibility(View.VISIBLE);
            holder.viewRoundInProcessGrey.setVisibility(View.GONE);
            holder.viewRoundShippedGreen.setVisibility(View.VISIBLE);
            holder.viewShippedGreen.setVisibility(View.VISIBLE);
            holder.viewShippedGrey.setVisibility(View.GONE);
            holder.viewRoundShippedGrey.setVisibility(View.GONE);
            holder.viewDeliveryGreen.setVisibility(View.GONE);
            holder.viewDeliveryGrey.setVisibility(View.VISIBLE);
            holder.viewRoundDeliveredGreen.setVisibility(View.GONE);
            holder.viewRoundDeliveredGrey.setVisibility(View.VISIBLE);
            holder.viewRoundDeliveredRed.setVisibility(View.GONE);
            holder.txtViewDelivered.setText("DELIVERED");
            holder.txtViewDelivered.setTextColor(mContext.getResources().getColor(R.color.md_grey_900));
            holder.btnCancel.setVisibility(View.VISIBLE);
            holder.btnReturn.setVisibility(View.GONE);
        } else if (orderProducts[position].orderStatus == 4) {
            holder.viewOrderStatusRed.setVisibility(View.GONE);
            holder.viewOrderStatusGreen.setVisibility(View.VISIBLE);
            holder.txtViewOrderStatus.setVisibility(View.VISIBLE);
            holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_teal_A700));
            holder.txtViewOrderStatus.setText("Order Delivered");
            holder.viewInProcessGreen.setVisibility(View.VISIBLE);
            holder.viewInProcessGrey.setVisibility(View.GONE);
            holder.viewRoundInProcessGreen.setVisibility(View.VISIBLE);
            holder.viewRoundInProcessGrey.setVisibility(View.GONE);
            holder.viewRoundShippedGreen.setVisibility(View.VISIBLE);
            holder.viewShippedGreen.setVisibility(View.VISIBLE);
            holder.viewShippedGrey.setVisibility(View.GONE);
            holder.viewRoundShippedGrey.setVisibility(View.GONE);
            holder.viewDeliveryGreen.setVisibility(View.VISIBLE);
            holder.viewDeliveryGrey.setVisibility(View.GONE);
            holder.viewRoundDeliveredGreen.setVisibility(View.VISIBLE);
            holder.viewRoundDeliveredGrey.setVisibility(View.GONE);
            holder.viewRoundDeliveredRed.setVisibility(View.GONE);
            holder.txtViewDelivered.setText("DELIVERED");
            holder.txtViewDelivered.setTextColor(mContext.getResources().getColor(R.color.md_grey_900));
            holder.btnCancel.setVisibility(View.GONE);
            holder.btnReturn.setVisibility(View.VISIBLE);
        } else if (orderProducts[position].orderStatus == 5) {

            if (orderProducts[position].refundRequest == 1) {
                holder.viewOrderStatusRed.setVisibility(View.VISIBLE);
                holder.viewOrderStatusGreen.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setVisibility(View.VISIBLE);
                holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
                holder.txtViewOrderStatus.setText("Cancelled");
            } else if (orderProducts[position].refundRequest == 2) {
                holder.viewOrderStatusRed.setVisibility(View.GONE);
                holder.viewOrderStatusGreen.setVisibility(View.VISIBLE);
                holder.txtViewOrderStatus.setVisibility(View.VISIBLE);
                holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_teal_A700));
                holder.txtViewOrderStatus.setText("Refund Request Sent");
            } else {
                holder.viewOrderStatusRed.setVisibility(View.GONE);
                holder.viewOrderStatusGreen.setVisibility(View.VISIBLE);
                holder.txtViewOrderStatus.setVisibility(View.VISIBLE);
                holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_teal_A700));
                holder.txtViewOrderStatus.setText("Amount Refunded");
            }

            holder.viewInProcessGreen.setVisibility(View.VISIBLE);
            holder.viewInProcessGrey.setVisibility(View.GONE);
            holder.viewRoundInProcessGreen.setVisibility(View.VISIBLE);
            holder.viewRoundInProcessGrey.setVisibility(View.GONE);
            holder.viewRoundShippedGreen.setVisibility(View.VISIBLE);
            holder.viewShippedGreen.setVisibility(View.VISIBLE);
            holder.viewShippedGrey.setVisibility(View.GONE);
            holder.viewRoundShippedGrey.setVisibility(View.GONE);
            holder.viewDeliveryGreen.setVisibility(View.VISIBLE);
            holder.viewDeliveryGrey.setVisibility(View.GONE);
            holder.viewRoundDeliveredGreen.setVisibility(View.GONE);
            holder.viewRoundDeliveredGrey.setVisibility(View.GONE);
            holder.viewRoundDeliveredRed.setVisibility(View.VISIBLE);
            holder.txtViewDelivered.setText("CANCELLED");
            holder.txtViewDelivered.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            holder.btnCancel.setVisibility(View.GONE);
            holder.btnReturn.setVisibility(View.GONE);
        } else if (orderProducts[position].orderStatus == 6) {
            holder.viewOrderStatusRed.setVisibility(View.VISIBLE);
            holder.viewOrderStatusGreen.setVisibility(View.GONE);
            holder.txtViewOrderStatus.setVisibility(View.VISIBLE);
            holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            holder.txtViewOrderStatus.setText("Return Request Received");
            holder.viewInProcessGreen.setVisibility(View.VISIBLE);
            holder.viewInProcessGrey.setVisibility(View.GONE);
            holder.viewRoundInProcessGreen.setVisibility(View.VISIBLE);
            holder.viewRoundInProcessGrey.setVisibility(View.GONE);
            holder.viewRoundShippedGreen.setVisibility(View.VISIBLE);
            holder.viewShippedGreen.setVisibility(View.VISIBLE);
            holder.viewShippedGrey.setVisibility(View.GONE);
            holder.viewRoundShippedGrey.setVisibility(View.GONE);
            holder.viewDeliveryGreen.setVisibility(View.VISIBLE);
            holder.viewDeliveryGrey.setVisibility(View.GONE);
            holder.viewRoundDeliveredGreen.setVisibility(View.VISIBLE);
            holder.viewRoundDeliveredGrey.setVisibility(View.GONE);
            holder.viewRoundDeliveredRed.setVisibility(View.GONE);
            holder.txtViewDelivered.setText("DELIVERED");
            holder.txtViewDelivered.setTextColor(mContext.getResources().getColor(R.color.md_grey_900));
            holder.btnCancel.setVisibility(View.GONE);
            holder.btnReturn.setVisibility(View.GONE);
        } else if (orderProducts[position].orderStatus == 7) {
            holder.viewOrderStatusRed.setVisibility(View.VISIBLE);
            holder.viewOrderStatusGreen.setVisibility(View.GONE);
            holder.txtViewOrderStatus.setVisibility(View.VISIBLE);
            holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            holder.txtViewOrderStatus.setText("Returned");
            holder.viewInProcessGreen.setVisibility(View.VISIBLE);
            holder.viewInProcessGrey.setVisibility(View.GONE);
            holder.viewRoundInProcessGreen.setVisibility(View.VISIBLE);
            holder.viewRoundInProcessGrey.setVisibility(View.GONE);
            holder.viewRoundShippedGreen.setVisibility(View.VISIBLE);
            holder.viewShippedGreen.setVisibility(View.VISIBLE);
            holder.viewShippedGrey.setVisibility(View.GONE);
            holder.viewRoundShippedGrey.setVisibility(View.GONE);
            holder.viewDeliveryGreen.setVisibility(View.VISIBLE);
            holder.viewDeliveryGrey.setVisibility(View.GONE);
            holder.viewRoundDeliveredGreen.setVisibility(View.VISIBLE);
            holder.viewRoundDeliveredGrey.setVisibility(View.GONE);
            holder.viewRoundDeliveredRed.setVisibility(View.GONE);
            holder.txtViewDelivered.setText("RETURNED");
            holder.txtViewDelivered.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            holder.btnCancel.setVisibility(View.GONE);
            holder.btnReturn.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return orderProducts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product_poster)
        ImageView imgViewProductPoster;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_tag_name)
        TextView txtViewTagName;
        @BindView(R.id.txt_view_size_qty)
        TextView txtViewSizeQty;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.txt_view_saved)
        TextView txtViewSaved;
        @BindView(R.id.view_in_process_green)
        View viewInProcessGreen;
        @BindView(R.id.view_in_process_grey)
        View viewInProcessGrey;
        @BindView(R.id.view_round_in_process_green)
        View viewRoundInProcessGreen;
        @BindView(R.id.view_round_in_process_grey)
        View viewRoundInProcessGrey;
        @BindView(R.id.view_shipped_green)
        View viewShippedGreen;
        @BindView(R.id.view_shipped_grey)
        View viewShippedGrey;
        @BindView(R.id.view_round_shipped_grey)
        View viewRoundShippedGrey;
        @BindView(R.id.view_round_shipped_green)
        View viewRoundShippedGreen;
        @BindView(R.id.view_delivery_green)
        View viewDeliveryGreen;
        @BindView(R.id.view_delivery_grey)
        View viewDeliveryGrey;
        @BindView(R.id.view_round_delivered_green)
        View viewRoundDeliveredGreen;
        @BindView(R.id.view_round_delivered_grey)
        View viewRoundDeliveredGrey;
        @BindView(R.id.view_round_delivered_red)
        View viewRoundDeliveredRed;
        @BindView(R.id.txt_view_order_status)
        TextView txtViewOrderStatus;
        @BindView(R.id.view_order_status_green)
        View viewOrderStatusGreen;
        @BindView(R.id.view_order_status_red)
        View viewOrderStatusRed;
        @BindView(R.id.txt_view_delivered)
        TextView txtViewDelivered;
        @BindView(R.id.btn_cancel)
        Button btnCancel;
        @BindView(R.id.btn_return)
        Button btnReturn;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
