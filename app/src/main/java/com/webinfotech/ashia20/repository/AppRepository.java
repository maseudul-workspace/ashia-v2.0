package com.webinfotech.ashia20.repository;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AppRepository {

    @POST("user/registration")
    @FormUrlEncoded
    Call<ResponseBody> registerUser(@Field("name") String name,
                                    @Field("email") String email,
                                    @Field("mobile") String mobile,
                                    @Field("dob") String DOB,
                                    @Field("gender") String gender,
                                    @Field("state") String state,
                                    @Field("city") String city,
                                    @Field("pin") String pin,
                                    @Field("address") String address,
                                    @Field("password") String password,
                                    @Field("confirm_password") String confirmPassword
    );

    @POST("user/login")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(@Field("user_id") String userId,
                                  @Field("password") String password
    );

    @GET("user/profile/{user_id}")
    Call<ResponseBody> fetchUserProfile(@Header("Authorization") String authorization,
                                        @Path("user_id") int userId
    );

    @POST("user/profile/update")
    @FormUrlEncoded
    Call<ResponseBody> updateUser(  @Header("Authorization") String authorization,
                                    @Field("user_id") int userId,
                                    @Field("name") String name,
                                    @Field("email") String email,
                                    @Field("mobile") String mobile,
                                    @Field("dob") String DOB,
                                    @Field("gender") String gender,
                                    @Field("state") String state,
                                    @Field("city") String city,
                                    @Field("pin") String pin,
                                    @Field("address") String address
    );

    @GET("user/shipping/list/{user_id}")
    Call<ResponseBody> fetchShippingAddressList(@Header("Authorization") String authorization,
                                                @Path("user_id") int userId
    );

    @POST("user/shipping/add")
    @FormUrlEncoded
    Call<ResponseBody> addShippingAddress(  @Header("Authorization") String authorization,
                                            @Field("user_id") int userId,
                                            @Field("name") String name,
                                            @Field("email") String email,
                                            @Field("mobile") String mobile,
                                            @Field("state") String state,
                                            @Field("city") String city,
                                            @Field("pin") String pin,
                                            @Field("address") String address
    );

    @GET("user/shipping/single/{user_id}/{address_id}")
    Call<ResponseBody> fetchShippingAddressDetails( @Header("Authorization") String authorization,
                                                    @Path("user_id") int userId,
                                                    @Path("address_id") int addressId
    );

    @POST("user/shipping/update")
    @FormUrlEncoded
    Call<ResponseBody> updateShippingAddress(   @Header("Authorization") String authorization,
                                                @Field("user_id") int userId,
                                                @Field("address_id") int addressId,
                                                @Field("name") String name,
                                                @Field("email") String email,
                                                @Field("mobile") String mobile,
                                                @Field("state") String state,
                                                @Field("city") String city,
                                                @Field("pin") String pin,
                                                @Field("address") String address
    );

    @GET("user/shipping/delete/{address_id}")
    Call<ResponseBody> deleteShippingAddress( @Header("Authorization") String authorization,
                                                    @Path("address_id") int addressId
    );

    @POST("user/change/password")
    @FormUrlEncoded
    Call<ResponseBody> changePassword(  @Header("Authorization") String authorization,
                                        @Field("user_id") int userId,
                                        @Field("current_pass") String currentPassword,
                                        @Field("new_password") String newPassword,
                                        @Field("confirm_password") String confirmPassword
    );

    @GET("app/load")
    Call<ResponseBody> fetchHomeData();

    @GET("sub/category/list/{cat_id}")
    Call<ResponseBody> fetchSubcategoryList(@Path("cat_id") int catId);

    @GET("last/category/list/{sub_cat_id}")
    Call<ResponseBody> fetchThirdSubcategoryList(@Path("sub_cat_id") int subCatId);

    @GET("product/list/{category_id}/{type}")
    Call<ResponseBody> fetchProductListWithFilter(   @Path("category_id") int categoryId,
                                                     @Path("type") int type
                                                     );

    @POST("product/filter")
    @FormUrlEncoded
    Call<ResponseBody> fetchProductListWithoutFilter(@Field("category_id") int categoryId,
                                                     @Field("page") int page,
                                                     @Field("type") int type,
                                                     @Field("brand[]") ArrayList<Integer> brands,
                                                     @Field("color[]") ArrayList<Integer> colors,
                                                     @Field("size[]") ArrayList<Integer> sizes,
                                                     @Field("price_from") int priceFrom,
                                                     @Field("price_to") int priceTo,
                                                     @Field("sort") int sort
                                                     );

    @GET("product/single/view/{product_id}")
    Call<ResponseBody> fetchProductDetails(@Path("product_id") int productId);

    @POST("cart/add")
    @FormUrlEncoded
    Call<ResponseBody> addToCart(  @Header("Authorization") String authorization,
                                   @Field("user_id") int userId,
                                   @Field("product_id") int productId,
                                   @Field("size_id") int sizeId,
                                   @Field("quantity") int quantity,
                                   @Field("color") String color
    );

    @GET("cart/fetch/{user_id}")
    Call<ResponseBody> fetchCartList( @Header("Authorization") String authorization,
                                      @Path("user_id") int userId
    );

    @POST("cart/update")
    @FormUrlEncoded
    Call<ResponseBody> updateCart(  @Header("Authorization") String authorization,
                                    @Field("cart_id") int cartId,
                                    @Field("size_id") int sizeId,
                                    @Field("quantity") int quantity
    );

    @GET("cart/remove/{cart_id}")
    Call<ResponseBody> removeCartItem(  @Header("Authorization") String authorization,
                                        @Path("cart_id") int cartId
    );

    @GET("checkout/coupons/{user_id}")
    Call<ResponseBody> fetchCoupons(  @Header("Authorization") String authorization,
                                      @Path("user_id") int userId
    );

    @POST("checkout/coupons/apply")
    @FormUrlEncoded
    Call<ResponseBody> applyCoupon(  @Header("Authorization") String authorization,
                                     @Field("user_id") int userId,
                                     @Field("coupon_code") String couponCode
                                     );

    @GET("charges/list")
    Call<ResponseBody> fetchShippingCharges();

    @POST("order/place")
    @FormUrlEncoded
    Call<ResponseBody> placeOrder(@Header("Authorization") String authorization,
                                  @Field("user_id") int userId,
                                  @Field("coupon_id") String couponId,
                                  @Field("payment_type") int paymentType,
                                  @Field("shipping_address_id") int shippingAddressId
    );

    @POST("order/payment/verify")
    @FormUrlEncoded
    Call<ResponseBody> verifyPayment(   @Header("Authorization") String authorization,
                                        @Field("user_id") int userId,
                                        @Field("razorpay_order_id") String razorpayOrderId,
                                        @Field("razorpay_payment_id") String razorpayPaymentId,
                                        @Field("razorpay_signature") String razorpaySignature,
                                        @Field("order_id") int orderId
    );

    @GET("order/history/{user_id}")
    Call<ResponseBody> fetchOrderHistory(  @Header("Authorization") String authorization,
                                           @Path("user_id") int userId
    );

    @GET("order/cancel/{order_item_id}")
    Call<ResponseBody> cancelOrder(  @Header("Authorization") String authorization,
                                     @Path("order_item_id") int orderItemId
    );

    @POST("order/cancel/refund")
    @FormUrlEncoded
    Call<ResponseBody> requestRefund(   @Header("Authorization") String authorization,
                                        @Field("order_item_id") int orderItemId,
                                        @Field("name") String name,
                                        @Field("bank_name") String bankName,
                                        @Field("branch_name") String branchName,
                                        @Field("ac_no") String accountNo,
                                        @Field("ifsc") String ifscCode
    );

    @POST("order/returd/refund")
    @FormUrlEncoded
    Call<ResponseBody> requestReturnRefund( @Header("Authorization") String authorization,
                                            @Field("order_item_id") int orderItemId,
                                            @Field("name") String name,
                                            @Field("bank_name") String bankName,
                                            @Field("branch_name") String branchName,
                                            @Field("ac_no") String accountNo,
                                            @Field("ifsc") String ifscCode
    );

    @GET("wish/list/add/{product_id}/{user_id}")
    Call<ResponseBody> addToWishlist(   @Header("Authorization") String authorization,
                                        @Path("user_id") int user_id,
                                        @Path("product_id") int productId
    );

    @GET("wish/list/item/remove/{wish_list_id}")
    Call<ResponseBody> removeFromWishlist(  @Header("Authorization") String authorization,
                                            @Path("wish_list_id") int wish_list_id);

    @GET("wish/list/items/{user_id}")
    Call<ResponseBody> fetchWishlist(  @Header("Authorization") String authorization,
                                       @Path("user_id") int userId);

    @GET("send/otp/{mobile}")
    Call<ResponseBody> sendOtp(@Path("mobile") String mobile);

    @POST("forgot/change/password")
    @FormUrlEncoded
    Call<ResponseBody> forgetPassword(  @Field("mobile") String mobile,
                                        @Field("new_password") String newPassword,
                                        @Field("confirm_password") String confirmPassword
    );

}
