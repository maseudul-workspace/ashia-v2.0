package com.webinfotech.ashia20.repository;

import android.util.Log;

import com.google.gson.Gson;
import com.webinfotech.ashia20.domain.models.CartDetailsWrapper;
import com.webinfotech.ashia20.domain.models.CommonResponse;
import com.webinfotech.ashia20.domain.models.CouponCodeWrapper;
import com.webinfotech.ashia20.domain.models.HomeDataWrapper;
import com.webinfotech.ashia20.domain.models.OrderPlaceResponse;
import com.webinfotech.ashia20.domain.models.OrdersWrapper;
import com.webinfotech.ashia20.domain.models.OtpResponse;
import com.webinfotech.ashia20.domain.models.ProductDetailsDataWrapper;
import com.webinfotech.ashia20.domain.models.ProductListWithFilterWrapper;
import com.webinfotech.ashia20.domain.models.ProductListWrapper;
import com.webinfotech.ashia20.domain.models.ShippingAddressDetailsWrapper;
import com.webinfotech.ashia20.domain.models.ShippingAddressWrapper;
import com.webinfotech.ashia20.domain.models.ShippingCharges;
import com.webinfotech.ashia20.domain.models.ShippingChargesWrapper;
import com.webinfotech.ashia20.domain.models.SubcategoryWrapper;
import com.webinfotech.ashia20.domain.models.ThirdCategoryDataWrapper;
import com.webinfotech.ashia20.domain.models.UserInfoWrapper;
import com.webinfotech.ashia20.domain.models.WishlistWrapper;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.Header;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = ApiClient.createService(AppRepository.class);
    }

    public CommonResponse registerUser(String name,
                                        String email,
                                        String mobile,
                                        String DOB,
                                        String gender,
                                        String state,
                                        String city,
                                        String pin,
                                        String address,
                                        String password,
                                        String confirmPassword
                                        ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.registerUser(name, email, mobile, DOB, gender, state, city, pin, address, password, confirmPassword);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Error Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public UserInfoWrapper checkLogin(String phone, String password){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkLogin(phone, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public UserInfoWrapper fetchUserProfile(String apiToken, int userId){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchUserProfile("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public CommonResponse updateUser(  String apiToken,
                                       int userId,
                                       String name,
                                       String email,
                                       String mobile,
                                       String DOB,
                                       String gender,
                                       String state,
                                       String city,
                                       String pin,
                                       String address
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateUser("Bearer " + apiToken, userId, name, email, mobile, DOB, gender, state, city, pin, address);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Error Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public ShippingAddressWrapper fetchShippingAddress(String apiToken, int userId){
        ShippingAddressWrapper shippingAddressWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchShippingAddressList("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Error Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    shippingAddressWrapper = null;
                }else{
                    shippingAddressWrapper = gson.fromJson(responseBody, ShippingAddressWrapper.class);
                }
            } else {
                shippingAddressWrapper = null;
            }
        }catch (Exception e){
            shippingAddressWrapper = null;
            Log.e("LogMsg", e.getMessage());
        }
        return shippingAddressWrapper;
    }

    public CommonResponse addShippingAddress(  String apiToken,
                                               int userId,
                                               String name,
                                               String email,
                                               String mobile,
                                               String state,
                                               String city,
                                               String pin,
                                               String address
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addShippingAddress("Bearer " + apiToken, userId, name, email, mobile, state, city, pin, address);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Error Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public ShippingAddressDetailsWrapper fetchShippingAddressDetails(String apiToken, int userId, int addressId){
        ShippingAddressDetailsWrapper shippingAddressWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchShippingAddressDetails("Bearer " + apiToken, userId, addressId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    shippingAddressWrapper = null;
                }else{
                    shippingAddressWrapper = gson.fromJson(responseBody, ShippingAddressDetailsWrapper.class);
                }
            } else {
                shippingAddressWrapper = null;
            }
        }catch (Exception e){
            shippingAddressWrapper = null;
            Log.e("LogMsg", "Error: " + e.getMessage());
        }
        return shippingAddressWrapper;
    }

    public CommonResponse updateShippingAddress(  String apiToken,
                                               int userId,
                                               int addressId,
                                               String name,
                                               String email,
                                               String mobile,
                                               String state,
                                               String city,
                                               String pin,
                                               String address
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateShippingAddress("Bearer " + apiToken, userId, addressId, name, email, mobile, state, city, pin, address);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Error Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse deleteShippingAddress(String apiToken, int addressId){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> delete = mRepository.deleteShippingAddress("Bearer " + apiToken, addressId);
            Response<ResponseBody> response = delete.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse changePassword(String apiToken, int userId, String currentPassword, String newPassword){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> change = mRepository.changePassword("Bearer " + apiToken, userId, currentPassword, newPassword, newPassword);
            Response<ResponseBody> response = change.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public HomeDataWrapper fetchHomeData() {
        HomeDataWrapper homeDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchHomeData();
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    homeDataWrapper = null;
                }else{
                    homeDataWrapper = gson.fromJson(responseBody, HomeDataWrapper.class);
                }
            } else {
                homeDataWrapper = null;
            }
        }catch (Exception e){
            homeDataWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return homeDataWrapper;
    }

    public SubcategoryWrapper fetchSubcategory(int catId) {
        SubcategoryWrapper subcategoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchSubcategoryList(catId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    subcategoryWrapper = null;
                }else{
                    subcategoryWrapper = gson.fromJson(responseBody, SubcategoryWrapper.class);
                }
            } else {
                subcategoryWrapper = null;
            }
        }catch (Exception e){
            subcategoryWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return subcategoryWrapper;
    }

    public ThirdCategoryDataWrapper fetchThirdSubcategory(int subCatId) {
        ThirdCategoryDataWrapper thirdCategoryDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchThirdSubcategoryList(subCatId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    thirdCategoryDataWrapper = null;
                }else{
                    thirdCategoryDataWrapper = gson.fromJson(responseBody, ThirdCategoryDataWrapper.class);
                }
            } else {
                thirdCategoryDataWrapper = null;
            }
        }catch (Exception e){
            thirdCategoryDataWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return thirdCategoryDataWrapper;
    }

    public ProductListWithFilterWrapper fetchProductListWithFilter(int categoryId, int type) {
        ProductListWithFilterWrapper productListWithFilterWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchProductListWithFilter(categoryId, type);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWithFilterWrapper = null;
                }else{
                    productListWithFilterWrapper = gson.fromJson(responseBody, ProductListWithFilterWrapper.class);
                }
            } else {
                productListWithFilterWrapper = null;
            }
        }catch (Exception e){
            productListWithFilterWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return productListWithFilterWrapper;
    }

    public ProductListWrapper fetchProductList(int categoryId,
                                               int page,
                                               int type,
                                               ArrayList<Integer> brands,
                                               ArrayList<Integer> colors,
                                               ArrayList<Integer> sizes,
                                               int priceFrom,
                                               int priceTo,
                                               int sort) {
        ProductListWrapper productListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchProductListWithoutFilter(categoryId, page, type, brands, colors, sizes, priceFrom, priceTo, sort);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWrapper = null;
                }else{
                    productListWrapper = gson.fromJson(responseBody, ProductListWrapper.class);
                }
            } else {
                productListWrapper = null;
            }
        }catch (Exception e){
            productListWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return productListWrapper;
    }

    public ProductDetailsDataWrapper fetchProductDetails(int productId) {
        Log.e("LogMsg", "Product Id: " + productId);
        ProductDetailsDataWrapper productDetailsDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchProductDetails(productId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productDetailsDataWrapper = null;
                }else{
                    productDetailsDataWrapper = gson.fromJson(responseBody, ProductDetailsDataWrapper.class);
                }
            } else {
                productDetailsDataWrapper = null;
            }
        }catch (Exception e){
            productDetailsDataWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return productDetailsDataWrapper;
    }

    public CommonResponse addToCart(String apiToken, int userId, int productId, int sizeId, int quantity, String colorCode) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addToCart("Bearer " + apiToken, userId, productId, sizeId, quantity, colorCode);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public CartDetailsWrapper fetchCartList(String apiToken, int userId) {
        CartDetailsWrapper cartDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCartList("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cartDetailsWrapper = null;
                }else{
                    cartDetailsWrapper = gson.fromJson(responseBody, CartDetailsWrapper.class);
                }
            } else {
                cartDetailsWrapper = null;
            }
        }catch (Exception e){
            cartDetailsWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return cartDetailsWrapper;
    }

    public CommonResponse updateCart(String authorization,
                                     int cartId,
                                     int sizeId,
                                     int quantity) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateCart("Bearer " + authorization, cartId, sizeId, quantity);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public CommonResponse removeCartItem(String authorization, int cartId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> remove = mRepository.removeCartItem("Bearer " + authorization, cartId);
            Response<ResponseBody> response = remove.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public CouponCodeWrapper fetchCoupons(String apiToken, int userId) {
        CouponCodeWrapper couponCodeWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCoupons("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    couponCodeWrapper = null;
                }else{
                    couponCodeWrapper = gson.fromJson(responseBody, CouponCodeWrapper.class);
                }
            } else {
                couponCodeWrapper = null;
            }
        }catch (Exception e){
            couponCodeWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return couponCodeWrapper;
    }

    public CouponCodeWrapper applyCoupon(String apiToken, int userId, String couponCode) {
        CouponCodeWrapper couponCodeWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> apply = mRepository.applyCoupon("Bearer " + apiToken, userId, couponCode);
            Response<ResponseBody> response = apply.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    couponCodeWrapper = null;
                }else{
                    couponCodeWrapper = gson.fromJson(responseBody, CouponCodeWrapper.class);
                }
            } else {
                couponCodeWrapper = null;
            }
        }catch (Exception e){
            couponCodeWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return couponCodeWrapper;
    }

    public ShippingChargesWrapper fetchShippingCharges() {
        ShippingChargesWrapper shippingChargesWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchShippingCharges();
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    shippingChargesWrapper = null;
                }else{
                    shippingChargesWrapper = gson.fromJson(responseBody, ShippingChargesWrapper.class);
                }
            } else {
                shippingChargesWrapper = null;
            }
        }catch (Exception e){
            shippingChargesWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return shippingChargesWrapper;
    }

    public OrderPlaceResponse placeOrder(String apiToken, int userId, String couponId, int paymentType, int shippingAddressId) {

        OrderPlaceResponse orderPlaceResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> place = mRepository.placeOrder("Bearer " + apiToken, userId, couponId, paymentType, shippingAddressId);
            Response<ResponseBody> response = place.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderPlaceResponse = null;
                }else{
                    orderPlaceResponse = gson.fromJson(responseBody, OrderPlaceResponse.class);
                }
            } else {
                orderPlaceResponse = null;
            }
        }catch (Exception e){
            orderPlaceResponse = null;
        }
        return orderPlaceResponse;
    }

    public CommonResponse verifyPayment( String authorization,
                                         int userId,
                                         String razorpayOrderId,
                                         String razorpayPaymentId,
                                         String razorpaySignature,
                                         int orderId) {
        Log.e("LogMsg", "Razorpay Order Id: " + razorpayOrderId);
        Log.e("LogMsg", "Razorpay Payment Id: " + razorpayPaymentId);
        Log.e("LogMsg", "Razorpay Signature: " + razorpaySignature);
        Log.e("LogMsg", "Order Id: " + orderId);

        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.verifyPayment("Bearer " + authorization, userId, razorpayOrderId, razorpayPaymentId, razorpaySignature, orderId);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public OrdersWrapper fetchOrderHistory(String apiToken, int userId) {
        OrdersWrapper ordersWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchOrderHistory("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    ordersWrapper = null;
                }else{
                    ordersWrapper = gson.fromJson(responseBody, OrdersWrapper.class);
                }
            } else {
                ordersWrapper = null;
            }
        }catch (Exception e){
            ordersWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return ordersWrapper;
    }

    public CommonResponse cancelOrder(String apiToken, int orderItemId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> cancel = mRepository.cancelOrder("Bearer " + apiToken, orderItemId);
            Response<ResponseBody> response = cancel.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public CommonResponse requestRefund(String apiToken, int orderItemId, String name, String bankName, String branchName, String accountNo, String ifscCode) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.requestRefund("Bearer " + apiToken, orderItemId, name, bankName, branchName, accountNo, ifscCode);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public CommonResponse requestReturnRefund(String apiToken, int orderItemId, String name, String bankName, String branchName, String accountNo, String ifscCode) {
        Log.e("LogMsg", "Order Item Id: " + orderItemId);
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.requestReturnRefund("Bearer " + apiToken, orderItemId, name, bankName, branchName, accountNo, ifscCode);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public CommonResponse addToWishlist(String apiToken, int userId, int productId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addToWishlist("Bearer " + apiToken, userId, productId);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public CommonResponse removeFromWishlist(String apiToken, int wishListId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> remove = mRepository.removeFromWishlist("Bearer " + apiToken, wishListId);
            Response<ResponseBody> response = remove.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public WishlistWrapper fetchWishlist(String apiToken, int userId) {
        WishlistWrapper wishlistWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchWishlist("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    wishlistWrapper = null;
                }else{
                    wishlistWrapper = gson.fromJson(responseBody, WishlistWrapper.class);
                }
            } else {
                wishlistWrapper = null;
            }
        }catch (Exception e){
            wishlistWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return wishlistWrapper;
    }

    public OtpResponse sendOtp(String phone) {
        OtpResponse otpResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> send = mRepository.sendOtp(phone);
            Response<ResponseBody> response = send.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    otpResponse = null;
                }else{
                    otpResponse = gson.fromJson(responseBody, OtpResponse.class);
                }
            } else {
                otpResponse = null;
            }
        }catch (Exception e){
            otpResponse = null;
        }
        return otpResponse;
    }

    public CommonResponse forgetPassword(String mobile, String password) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> forget = mRepository.forgetPassword(mobile, password, password);
            Response<ResponseBody> response = forget.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

}
