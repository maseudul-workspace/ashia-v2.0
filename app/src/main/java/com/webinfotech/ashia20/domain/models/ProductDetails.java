package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetails {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("main_image")
    @Expose
    public String mainImage;

    @SerializedName("min_price")
    @Expose
    public double minPrice;

    @SerializedName("mrp")
    @Expose
    public double mrp;

    @SerializedName("short_description")
    @Expose
    public String shortDescription;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("size_chart")
    @Expose
    public String sizeChart;

    @SerializedName("category")
    @Expose
    public Category category;

    @SerializedName("sub_category")
    @Expose
    public Subcategory subcategory;

    @SerializedName("third_category")
    @Expose
    public ThirdCategory thirdCategory;

    @SerializedName("brand_name")
    @Expose
    public String brandName;

    @SerializedName("sizes")
    @Expose
    public Size[] sizes;

    @SerializedName("productColors")
    @Expose
    public Color[] colors;

    @SerializedName("productImages")
    @Expose
    public Image[] images;

    public boolean isWishList = false;

}
