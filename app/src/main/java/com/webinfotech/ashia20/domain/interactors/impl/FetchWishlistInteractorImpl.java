package com.webinfotech.ashia20.domain.interactors.impl;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.FetchWishlistInteractor;
import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;
import com.webinfotech.ashia20.domain.models.CommonResponse;
import com.webinfotech.ashia20.domain.models.Wishlist;
import com.webinfotech.ashia20.domain.models.WishlistWrapper;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

public class FetchWishlistInteractorImpl extends AbstractInteractor implements FetchWishlistInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchWishlistInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishlistFetchFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Wishlist[] wishlists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishlistFetchSuccess(wishlists);
            }
        });
    }

    @Override
    public void run() {
        final WishlistWrapper wishlistWrapper = mRepository.fetchWishlist(apiToken, userId);
        if (wishlistWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!wishlistWrapper.status) {
            notifyError(wishlistWrapper.message, wishlistWrapper.login_error);
        } else {
            postMessage(wishlistWrapper.wishlists);
        }
    }
}
