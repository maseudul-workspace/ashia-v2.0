package com.webinfotech.ashia20.domain.interactors.impl;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.UpdateAddressInteractor;
import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;
import com.webinfotech.ashia20.domain.models.CommonResponse;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

public class UpdateAddressInteractorImpl extends AbstractInteractor implements UpdateAddressInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;
    int addressId;
    String name;
    String email;
    String mobile;
    String state;
    String city;
    String pin;
    String address;

    public UpdateAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken, int addressId, String name, String email, String mobile, String state, String city, String pin, String address) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
        this.addressId = addressId;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.state = state;
        this.city = city;
        this.pin = pin;
        this.address = address;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateAddressFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateAddressSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateShippingAddress(apiToken, userId, addressId, name, email, mobile, state, city, pin, address);
        if (commonResponse == null) {
            notifyError("Slow Internet Connection", commonResponse.login_error);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
