package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.UserInfo;

public interface CheckLoginInteractor {
    interface Callback {
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
