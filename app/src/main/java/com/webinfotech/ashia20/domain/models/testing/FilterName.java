package com.webinfotech.ashia20.domain.models.testing;

import java.util.ArrayList;

public class FilterName {

    public int filterNameId;
    public String filterName;
    public ArrayList<FilterValue> filterValues;
    public boolean isSelected;

    public FilterName(int filterNameId, String filterName, ArrayList<FilterValue> filterValues, boolean isSelected) {
        this.filterNameId = filterNameId;
        this.filterName = filterName;
        this.filterValues = filterValues;
        this.isSelected = isSelected;
    }
}
