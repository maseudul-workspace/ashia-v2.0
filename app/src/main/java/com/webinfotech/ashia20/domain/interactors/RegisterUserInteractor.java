package com.webinfotech.ashia20.domain.interactors;

public interface RegisterUserInteractor {
    interface Callback {
        void onRegisterSuccess();
        void onRegisterFail(String errorMsg);
    }
}
