package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Color {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String colorName;

    @SerializedName("code")
    @Expose
    public String colorCode;

    public boolean isSelected = false;

}
