package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.Category;
import com.webinfotech.ashia20.domain.models.Subcategory;
import com.webinfotech.ashia20.domain.models.SubcategoryData;

public interface FetchSubcategoryInteractor {
    interface Callback {
        void onGettingSubcategorySuccess(SubcategoryData subcategoryData);
        void onGettingSubcategoryFail(String errorMsg);
    }
}
