package com.webinfotech.ashia20.domain.interactors;

public interface AddToCartInteractor {
    interface Callback {
        void onAddToCartSuccess();
        void onAddToCartFail(String errorMsg, int loginError);
    }
}
