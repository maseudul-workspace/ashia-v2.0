package com.webinfotech.ashia20.domain.models.testing;

public class Review {

    public String review;
    public String userName;
    public String date;

    public Review(String review, String userName, String date) {
        this.review = review;
        this.userName = userName;
        this.date = date;
    }
}
