package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("is_sub_category")
    @Expose
    public int isSubCategory;

    @SerializedName("sub_category")
    @Expose
    public Subcategory[] subcategories;

}
