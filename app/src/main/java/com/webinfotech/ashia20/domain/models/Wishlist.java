package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wishlist {

    @SerializedName("wish_list_id")
    @Expose
    public int wishListId;

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("product")
    @Expose
    public Product product;

}
