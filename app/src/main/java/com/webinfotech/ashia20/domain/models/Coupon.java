package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Coupon {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("coupon")
    @Expose
    public String coupon;

    @SerializedName("discount")
    @Expose
    public int discount;

    @SerializedName("description")
    @Expose
    public String description;

}
