package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Size {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("mrp")
    @Expose
    public double mrp;

    @SerializedName("price")
    @Expose
    public double price;

    @SerializedName("stock")
    @Expose
    public int stock;

    public boolean isSelected = false;

}
