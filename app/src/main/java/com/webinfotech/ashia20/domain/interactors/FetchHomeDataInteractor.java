package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.HomeData;

public interface FetchHomeDataInteractor {
    interface Callback {
        void onGettingHomeDataSuccess(HomeData homeData);
        void onGettingHomeDataFail(String errorMsg);
    }
}
