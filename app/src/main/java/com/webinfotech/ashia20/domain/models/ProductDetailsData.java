package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailsData {

    @SerializedName("product")
    @Expose
    public ProductDetails productDetails;

    @SerializedName("related_products")
    @Expose
    public Product[] relatedProducts;

}
