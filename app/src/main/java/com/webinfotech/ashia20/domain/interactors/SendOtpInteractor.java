package com.webinfotech.ashia20.domain.interactors;

public interface SendOtpInteractor {
    interface Callback {
        void onSendOtpSuccess(String otp);
        void onSendOtpFail(String errorMsg);
    }
}
