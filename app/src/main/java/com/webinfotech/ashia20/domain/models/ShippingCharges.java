package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShippingCharges {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("amount")
    @Expose
    public double amount;

}
