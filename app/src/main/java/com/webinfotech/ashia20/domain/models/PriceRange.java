package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceRange {

    @SerializedName("price_from")
    @Expose
    public float priceForm;

    @SerializedName("price_to")
    @Expose
    public float priceTo;

}
