package com.webinfotech.ashia20.domain.interactors;

public interface RequestRefundInteractor {
    interface Callback {
        void onRequestRefundSuccess();
        void onRequestRefundFail(String errorMsg, int loginError);
    }
}
