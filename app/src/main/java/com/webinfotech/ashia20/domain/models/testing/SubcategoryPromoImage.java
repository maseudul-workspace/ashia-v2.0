package com.webinfotech.ashia20.domain.models.testing;

public class SubcategoryPromoImage {

    public String image;
    public String text;

    public SubcategoryPromoImage(String image, String text) {
        this.image = image;
        this.text = text;
    }
}
