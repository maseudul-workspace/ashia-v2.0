package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.ShippingAddress;

public interface FetchShippingAddressInteractor {
    interface Callback {
        void onGettingAddressListSucces(ShippingAddress[] shippingAddresses);
        void onGettingAddressListFail(String errorMsg, int loginError);
    }
}
