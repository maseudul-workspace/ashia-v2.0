package com.webinfotech.ashia20.domain.models.testing;

public class Category {

    public String name;
    public String image;

    public Category(String name, String image) {
        this.name = name;
        this.image = image;
    }
}
