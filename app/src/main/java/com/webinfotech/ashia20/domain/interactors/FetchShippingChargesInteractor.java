package com.webinfotech.ashia20.domain.interactors;

public interface FetchShippingChargesInteractor {
    interface Callback {
        void onShippingChargeFetchSuccess(double minAmount, double shippingCharge);
        void onShippingChargeFetchFail();
    }
}
