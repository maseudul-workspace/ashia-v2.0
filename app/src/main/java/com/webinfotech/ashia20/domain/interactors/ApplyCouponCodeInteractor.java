package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.Coupon;

public interface ApplyCouponCodeInteractor {
    interface Callback {
        void onCouponAppliedSuccess(Coupon coupon);
        void onCouponAppliedFail(String errorMsg, int loginError);
    }
}
