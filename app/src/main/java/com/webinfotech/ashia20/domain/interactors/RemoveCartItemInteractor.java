package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;

public interface RemoveCartItemInteractor {
    interface Callback {
        void onRemoveCartItemSuccess();
        void onRemoveCartItemFail(String errorMsg, int loginError);
    }
}
