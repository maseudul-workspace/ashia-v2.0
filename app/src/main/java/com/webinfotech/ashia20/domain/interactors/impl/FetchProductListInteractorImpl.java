package com.webinfotech.ashia20.domain.interactors.impl;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.FetchProductListIntercator;
import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;
import com.webinfotech.ashia20.domain.models.Product;
import com.webinfotech.ashia20.domain.models.ProductListData;
import com.webinfotech.ashia20.domain.models.ProductListWrapper;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

import java.util.ArrayList;

public class FetchProductListInteractorImpl extends AbstractInteractor implements FetchProductListIntercator {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int categoryId;
    int page;
    int type;
    ArrayList<Integer> brands;
    ArrayList<Integer> colors;
    ArrayList<Integer> sizes;
    int priceFrom;
    int priceTo;
    int sort;

    public FetchProductListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int categoryId, int page, int type, ArrayList<Integer> brands, ArrayList<Integer> colors, ArrayList<Integer> sizes, int priceFrom, int priceTo, int sort) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.categoryId = categoryId;
        this.page = page;
        this.type = type;
        this.brands = brands;
        this.colors = colors;
        this.sizes = sizes;
        this.priceFrom = priceFrom;
        this.priceTo = priceTo;
        this.sort = sort;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListFail(errorMsg);
            }
        });
    }

    private void postMessage(Product[] products, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListSuccess(products, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWrapper productListWrapper = mRepository.fetchProductList(categoryId, page, type, brands, colors, sizes, priceFrom, priceTo, sort);
        if (productListWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!productListWrapper.status) {
            notifyError(productListWrapper.message);
        } else {
            postMessage(productListWrapper.products, productListWrapper.totalPage);
        }
    }
}
