package com.webinfotech.ashia20.domain.models.testing;

public class Brand {

    public String brandMain;
    public String brand;

    public Brand(String brandMain, String brand) {
        this.brandMain = brandMain;
        this.brand = brand;
    }
}
