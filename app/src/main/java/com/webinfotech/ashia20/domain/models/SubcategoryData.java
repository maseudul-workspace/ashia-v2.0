package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubcategoryData {

    @SerializedName("main_category_image")
    @Expose
    public Image[] images;

    @SerializedName("sub_category")
    @Expose
    public Subcategory[] subcategories;

}
