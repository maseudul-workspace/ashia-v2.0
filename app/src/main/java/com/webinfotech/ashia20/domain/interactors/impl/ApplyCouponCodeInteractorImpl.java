package com.webinfotech.ashia20.domain.interactors.impl;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.ApplyCouponCodeInteractor;
import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;
import com.webinfotech.ashia20.domain.models.Coupon;
import com.webinfotech.ashia20.domain.models.CouponCodeWrapper;
import com.webinfotech.ashia20.repository.AppRepository;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

public class ApplyCouponCodeInteractorImpl extends AbstractInteractor implements ApplyCouponCodeInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    String couponCode;

    public ApplyCouponCodeInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String aoiToken, int userId, String couponCode) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = aoiToken;
        this.userId = userId;
        this.couponCode = couponCode;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCouponAppliedFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Coupon coupon){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCouponAppliedSuccess(coupon);
            }
        });
    }

    @Override
    public void run() {
        final CouponCodeWrapper couponCodeWrapper = mRepository.applyCoupon(apiToken, userId, couponCode);
        if (couponCodeWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!couponCodeWrapper.status) {
            notifyError(couponCodeWrapper.message, couponCodeWrapper.login_error);
        } else {
            postMessage(couponCodeWrapper.coupon);
        }
    }
}
