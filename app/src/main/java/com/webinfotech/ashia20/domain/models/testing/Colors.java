package com.webinfotech.ashia20.domain.models.testing;

public class Colors {

    public int colorId;
    public String colorName;
    public String color;

    public Colors(int colorId, String colorName, String color) {
        this.colorId = colorId;
        this.colorName = colorName;
        this.color = color;
    }
}
