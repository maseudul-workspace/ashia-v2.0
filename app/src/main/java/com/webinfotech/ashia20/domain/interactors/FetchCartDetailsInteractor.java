package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.CartDetails;

public interface FetchCartDetailsInteractor {
    interface Callback {
        void onGettingCartDetailsSuccess(CartDetails[] cartDetails);
        void onGettingCartDetailsFail(String errorMsg, int loginError);
    }
}
