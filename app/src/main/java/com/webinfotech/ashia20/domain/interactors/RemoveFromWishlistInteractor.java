package com.webinfotech.ashia20.domain.interactors;

public interface RemoveFromWishlistInteractor {
    interface Callback {
        void onRemoveFromWishlistSuccess();
        void onRemoveFromWishlistFail(String errorMsg, int loginError);
    }
}
