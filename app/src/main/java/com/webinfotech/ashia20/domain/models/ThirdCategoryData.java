package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThirdCategoryData {

    @SerializedName("second_category_image")
    @Expose
    public Image[] images;

    @SerializedName("third_category")
    @Expose
    public ThirdCategory[] thirdCategories;

}
