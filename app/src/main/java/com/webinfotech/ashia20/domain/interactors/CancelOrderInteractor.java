package com.webinfotech.ashia20.domain.interactors;

public interface CancelOrderInteractor {
    interface Callback {
        void onOrderCancelSuccess();
        void onOrderCancelFail(String errorMsg, int loginError);
    }
}
