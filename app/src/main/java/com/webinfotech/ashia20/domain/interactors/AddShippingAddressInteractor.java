package com.webinfotech.ashia20.domain.interactors;

public interface AddShippingAddressInteractor {
    interface Callback {
        void onAddShippingAddressSuccess();
        void onAddShippingAddressFail(String errorMsg, int loginError);
    }
}
