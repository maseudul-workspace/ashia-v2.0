package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.PaymentDataMain;

public interface PlaceOrderInteractor {
    interface Callback {
        void onPlaceOrderSuccess(PaymentDataMain paymentDataMain);
        void onPlaceOrderFail(String errorMsg, int loginError);
    }
}
