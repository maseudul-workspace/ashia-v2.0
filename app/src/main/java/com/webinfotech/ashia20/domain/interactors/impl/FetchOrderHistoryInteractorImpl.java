package com.webinfotech.ashia20.domain.interactors.impl;

import android.content.Context;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.FetchOrderHistoryInteractor;
import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;
import com.webinfotech.ashia20.domain.models.Orders;
import com.webinfotech.ashia20.domain.models.OrdersWrapper;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

public class FetchOrderHistoryInteractorImpl extends AbstractInteractor implements FetchOrderHistoryInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchOrderHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistoryFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Orders[] orders){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistorySuccess(orders);
            }
        });
    }

    @Override
    public void run() {
        OrdersWrapper ordersWrapper = mRepository.fetchOrderHistory(apiToken, userId);
        if (ordersWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!ordersWrapper.status) {
            notifyError(ordersWrapper.message, ordersWrapper.login_error);
        } else {
            postMessage(ordersWrapper.orders);
        }
    }
}
