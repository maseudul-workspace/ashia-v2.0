package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.Product;

public interface FetchProductListIntercator {
    interface Callback {
        void onGettingProductListSuccess(Product[] products, int totalPage);
        void onGettingProductListFail(String errorMsg);
    }
}
