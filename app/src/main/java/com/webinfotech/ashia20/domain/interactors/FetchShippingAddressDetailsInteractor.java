package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.ShippingAddress;

public interface FetchShippingAddressDetailsInteractor {
    interface Callback {
        void onGettingAddressDetailsSuccess(ShippingAddress shippingAddress);
        void onGettingAddressDetailsFail(String errorMsg, int loginError);
    }
}
