package com.webinfotech.ashia20.domain.models.testing;

public class Product {

    public String productName;
    public String productBrand;
    public String productImage;
    public String productPrice;
    public String productMrp;
    public String productDiscount;

    public Product(String productName, String productBrand, String productImage, String productPrice, String productMrp, String productDiscount) {
        this.productName = productName;
        this.productBrand = productBrand;
        this.productImage = productImage;
        this.productPrice = productPrice;
        this.productMrp = productMrp;
        this.productDiscount = productDiscount;
    }
}
