package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductListData {

    @SerializedName("filters")
    @Expose
    public FilterName[] filterNames;

    @SerializedName("price_range")
    @Expose
    public PriceRange priceRange;

    @SerializedName("product")
    @Expose
    public Product[] products;

}
