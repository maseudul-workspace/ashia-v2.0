package com.webinfotech.ashia20.domain.interactors.impl;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.PlaceOrderInteractor;
import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;
import com.webinfotech.ashia20.domain.models.OrderPlaceResponse;
import com.webinfotech.ashia20.domain.models.PaymentDataMain;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

public class PlaceOrderInteractorImpl extends AbstractInteractor implements PlaceOrderInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    String couponId;
    int paymentType;
    int shippingAddressId;

    public PlaceOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, String couponId, int paymentType, int shippingAddressId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.couponId = couponId;
        this.paymentType = paymentType;
        this.shippingAddressId = shippingAddressId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(PaymentDataMain paymentDataMain){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderSuccess(paymentDataMain);
            }
        });
    }

    @Override
    public void run() {
        final OrderPlaceResponse orderPlaceResponse = mRepository.placeOrder(apiToken, userId, couponId, paymentType, shippingAddressId);
        if (orderPlaceResponse == null) {
            notifyError("Please Internet Connection", 0);
        } else if (!orderPlaceResponse.status) {
            notifyError(orderPlaceResponse.message, orderPlaceResponse.login_error);
        } else {
            postMessage(orderPlaceResponse.paymentDataMain);
        }
    }
}
