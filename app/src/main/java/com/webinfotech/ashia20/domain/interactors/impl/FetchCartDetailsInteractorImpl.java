package com.webinfotech.ashia20.domain.interactors.impl;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.FetchCartDetailsInteractor;
import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;
import com.webinfotech.ashia20.domain.models.CartDetails;
import com.webinfotech.ashia20.domain.models.CartDetailsWrapper;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

public class FetchCartDetailsInteractorImpl extends AbstractInteractor implements FetchCartDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchCartDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartDetailsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(CartDetails[] cartDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartDetailsSuccess(cartDetails);
            }
        });
    }

    @Override
    public void run() {
        final CartDetailsWrapper cartDetailsWrapper = mRepository.fetchCartList(apiToken, userId);
        if (cartDetailsWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!cartDetailsWrapper.status) {
            notifyError(cartDetailsWrapper.message, cartDetailsWrapper.login_error);
        } else {
            postMessage(cartDetailsWrapper.cartDetails);
        }
    }
}
