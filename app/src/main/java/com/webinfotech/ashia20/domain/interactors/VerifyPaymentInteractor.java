package com.webinfotech.ashia20.domain.interactors;

public interface VerifyPaymentInteractor {
    interface Callback {
        void onVerifyPaymentSuccess();
        void onVerifyPaymentFail(String errorMsg);
    }
}
