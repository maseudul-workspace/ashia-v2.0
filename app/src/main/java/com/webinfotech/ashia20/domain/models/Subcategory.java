package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subcategory {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("category_id")
    @Expose
    public int categoryId;

    @SerializedName("is_sub_category")
    @Expose
    public int isSubCategory;

    @SerializedName("status")
    @Expose
    public int status;


    @SerializedName("third_category")
    @Expose
    public ThirdCategory[] thirdCategories;

}
