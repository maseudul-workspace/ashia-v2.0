package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.Orders;

public interface FetchOrderHistoryInteractor {
    interface Callback {
        void onGettingOrderHistorySuccess(Orders[] orders);
        void onGettingOrderHistoryFail(String errorMsg, int loginError);
    }
}
