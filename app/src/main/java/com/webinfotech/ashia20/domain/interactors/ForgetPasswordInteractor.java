package com.webinfotech.ashia20.domain.interactors;

public interface ForgetPasswordInteractor {
    interface Callback {
        void onForgetPasswordSuccess();
        void onForgetPasswordFail(String errorMsg);
    }
}
