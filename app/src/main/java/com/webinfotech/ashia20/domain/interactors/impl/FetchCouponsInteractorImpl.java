package com.webinfotech.ashia20.domain.interactors.impl;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.FetchCouponsInteractor;
import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;
import com.webinfotech.ashia20.domain.models.Coupon;
import com.webinfotech.ashia20.domain.models.CouponCodeWrapper;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

public class FetchCouponsInteractorImpl extends AbstractInteractor implements FetchCouponsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchCouponsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCouponFetchFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Coupon coupon){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCouponFetchSuccess(coupon);
            }
        });
    }

    @Override
    public void run() {
        final CouponCodeWrapper couponCodeWrapper = mRepository.fetchCoupons(apiToken, userId);
        if (couponCodeWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!couponCodeWrapper.status) {
            notifyError(couponCodeWrapper.message, couponCodeWrapper.login_error);
        } else {
            postMessage(couponCodeWrapper.coupon);
        }
    }
}
