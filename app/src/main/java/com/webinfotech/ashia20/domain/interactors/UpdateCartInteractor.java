package com.webinfotech.ashia20.domain.interactors;

public interface UpdateCartInteractor {
    interface Callback {
        void onUpdateCartSuccess();
        void onUpdateCartFail(String errorMsg, int loginError);
    }
}
