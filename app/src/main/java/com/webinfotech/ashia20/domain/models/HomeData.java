package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeData {

    @SerializedName("slider")
    @Expose
    public Image[] sliders;

    @SerializedName("category")
    @Expose
    public Category[] categories;

    @SerializedName("banner")
    @Expose
    public Image banner;

    @SerializedName("brands")
    @Expose
    public Brands[] brands;

}
