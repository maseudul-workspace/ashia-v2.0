package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Orders {

    @SerializedName("id")
    @Expose
    public int orderId;

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("discount")
    @Expose
    public double discount;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("shipping_charge")
    @Expose
    public double shippingCharge;

    @SerializedName("total_amount")
    @Expose
    public double totalAmount;

    @SerializedName("payment_type")
    @Expose
    public int paymentType;

    @SerializedName("payment_status")
    @Expose
    public int paymentStatus;

    @SerializedName("order_status")
    @Expose
    public int orderStatus;

    @SerializedName("shipping_address")
    @Expose
    public ShippingAddress shippingAddress;

    @SerializedName("items")
    @Expose
    public OrderProducts[] orderProducts;

}
