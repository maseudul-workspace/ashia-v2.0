package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.ThirdCategoryData;

public interface FetchThirdCategoryInteractor {
    interface Callback {
        void onGettingDataSuccess(ThirdCategoryData thirdCategoryData);
        void onGettingDataFail(String errorMsg);
    }
}
