package com.webinfotech.ashia20.domain.interactors.impl;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.FetchSubcategoryInteractor;
import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;
import com.webinfotech.ashia20.domain.models.Category;
import com.webinfotech.ashia20.domain.models.CategoryWrapper;
import com.webinfotech.ashia20.domain.models.ShippingAddress;
import com.webinfotech.ashia20.domain.models.Subcategory;
import com.webinfotech.ashia20.domain.models.SubcategoryData;
import com.webinfotech.ashia20.domain.models.SubcategoryWrapper;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

public class FetchSubcategoryInteractorImpl extends AbstractInteractor implements FetchSubcategoryInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int categoryId;

    public FetchSubcategoryInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int categoryId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.categoryId = categoryId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSubcategoryFail(errorMsg);
            }
        });
    }

    private void postMessage(SubcategoryData subcategoryData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSubcategorySuccess(subcategoryData);
            }
        });
    }

    @Override
    public void run() {
        final SubcategoryWrapper subcategoryWrapper = mRepository.fetchSubcategory(categoryId);
        if (subcategoryWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!subcategoryWrapper.status) {
            notifyError(subcategoryWrapper.message);
        } else {
            postMessage(subcategoryWrapper.subcategoryData);
        }
    }
}
