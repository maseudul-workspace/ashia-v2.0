package com.webinfotech.ashia20.domain.interactors.impl;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.FetchProductListWithFilterInteractor;
import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;
import com.webinfotech.ashia20.domain.models.ProductListData;
import com.webinfotech.ashia20.domain.models.ProductListWithFilterWrapper;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

public class FetchProductListWithFilterInteractorImpl extends AbstractInteractor implements FetchProductListWithFilterInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int categoryId;
    int type;

    public FetchProductListWithFilterInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int categoryId, int type) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.categoryId = categoryId;
        this.type = type;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListWithFilterFail(errorMsg);
            }
        });
    }

    private void postMessage(ProductListData productListData, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListWithFilterSuccess(productListData, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWithFilterWrapper productListWithFilterWrapper = mRepository.fetchProductListWithFilter(categoryId, type);
        if (productListWithFilterWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!productListWithFilterWrapper.status) {
            notifyError(productListWithFilterWrapper.message);
        } else {
            postMessage(productListWithFilterWrapper.productListData, productListWithFilterWrapper.totalPage);
        }
    }
}
