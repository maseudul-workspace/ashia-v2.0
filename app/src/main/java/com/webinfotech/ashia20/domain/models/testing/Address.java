package com.webinfotech.ashia20.domain.models.testing;

public class Address {

    public String userName;
    public String address;
    public String village;
    public String district;
    public String pin;
    public String mobile;

    public Address(String userName, String address, String village, String district, String pin, String mobile) {
        this.userName = userName;
        this.address = address;
        this.village = village;
        this.district = district;
        this.pin = pin;
        this.mobile = mobile;
    }
}
