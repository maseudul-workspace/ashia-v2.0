package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.Coupon;

public interface FetchCouponsInteractor {
    interface Callback {
        void onCouponFetchSuccess(Coupon coupon);
        void onCouponFetchFail(String errorMsg, int loginError);
    }
}
