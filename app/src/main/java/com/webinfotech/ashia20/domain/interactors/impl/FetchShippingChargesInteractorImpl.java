package com.webinfotech.ashia20.domain.interactors.impl;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.FetchShippingChargesInteractor;
import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;
import com.webinfotech.ashia20.domain.models.ShippingCharges;
import com.webinfotech.ashia20.domain.models.ShippingChargesWrapper;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

public class FetchShippingChargesInteractorImpl extends AbstractInteractor implements FetchShippingChargesInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;

    public FetchShippingChargesInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onShippingChargeFetchFail();
            }
        });
    }

    private void postMessage(double minAmount, double charges){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onShippingChargeFetchSuccess(minAmount, charges);
            }
        });
    }

    @Override
    public void run() {
        final ShippingChargesWrapper shippingChargesWrapper = mRepository.fetchShippingCharges();
        if (shippingChargesWrapper == null) {
            notifyError();
        } else {
            ShippingCharges[] shippingCharges = shippingChargesWrapper.charges;
            postMessage(shippingCharges[0].amount, shippingCharges[1].amount);
        }
    }
}
