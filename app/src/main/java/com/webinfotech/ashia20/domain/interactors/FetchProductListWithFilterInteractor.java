package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.ProductListData;

public interface FetchProductListWithFilterInteractor {
    interface Callback {
        void onGettingProductListWithFilterSuccess(ProductListData productListData, int totalPage);
        void onGettingProductListWithFilterFail(String errorMsg);
    }
}
