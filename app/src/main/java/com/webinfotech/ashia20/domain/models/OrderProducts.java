package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderProducts {

    @SerializedName("order_item_id")
    @Expose
    public int orderItemId;

    @SerializedName("product_name")
    @Expose
    public String productName;

    @SerializedName("product_image")
    @Expose
    public String productImage;

    @SerializedName("size")
    @Expose
    public String size;

    @SerializedName("color")
    @Expose
    public String color;

    @SerializedName("quantity")
    @Expose
    public String quantity;

    @SerializedName("price")
    @Expose
    public double price;

    @SerializedName("mrp")
    @Expose
    public double mrp;

    @SerializedName("order_status")
    @Expose
    public int orderStatus;

    @SerializedName("refund_request")
    @Expose
    public int refundRequest;

    @SerializedName("refund_amount")
    @Expose
    public double refundAmount;

}
