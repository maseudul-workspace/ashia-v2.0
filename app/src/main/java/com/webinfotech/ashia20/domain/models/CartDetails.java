package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartDetails {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("quantity")
    @Expose
    public int quantity;

    @SerializedName("color")
    @Expose
    public String color;

    @SerializedName("size")
    @Expose
    public Size size;

    @SerializedName("product")
    @Expose
    public ProductDetails productDetails;

}
