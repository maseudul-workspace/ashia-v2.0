package com.webinfotech.ashia20.domain.interactors.impl;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.FetchThirdCategoryInteractor;
import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;
import com.webinfotech.ashia20.domain.models.SubcategoryData;
import com.webinfotech.ashia20.domain.models.ThirdCategoryData;
import com.webinfotech.ashia20.domain.models.ThirdCategoryDataWrapper;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

public class FetchThirdCategoryInteractorImpl extends AbstractInteractor implements FetchThirdCategoryInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int subCatId;

    public FetchThirdCategoryInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int subCatId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.subCatId = subCatId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDataFail(errorMsg);
            }
        });
    }

    private void postMessage(ThirdCategoryData thirdCategoryData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDataSuccess(thirdCategoryData);
            }
        });
    }

    @Override
    public void run() {
        final ThirdCategoryDataWrapper thirdCategoryDataWrapper = mRepository.fetchThirdSubcategory(subCatId);
        if (thirdCategoryDataWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!thirdCategoryDataWrapper.status) {
            notifyError(thirdCategoryDataWrapper.message);
        } else {
            postMessage(thirdCategoryDataWrapper.thirdCategoryData);
        }
    }
}
