package com.webinfotech.ashia20.domain.interactors;

public interface RequestReturnRefundInteractor {
    interface Callback {
        void onRequestReturnRefundSuccess();
        void onRequestReturnRefundFail(String errorMsg, int loginError);
    }
}
