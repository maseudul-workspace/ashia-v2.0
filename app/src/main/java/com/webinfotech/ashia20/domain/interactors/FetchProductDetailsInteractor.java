package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.ProductDetailsData;

public interface FetchProductDetailsInteractor {
    interface Callback {
        void onGettingProductDetailsSuccess(ProductDetailsData productDetailsData);
        void onGettingProductDetailsFail(String errorMsg);
    }
}
