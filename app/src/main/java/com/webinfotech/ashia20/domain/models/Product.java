package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("main_image")
    @Expose
    public String mainImage;

    @SerializedName("min_price")
    @Expose
    public double minPrice;

    @SerializedName("mrp")
    @Expose
    public double mrp;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("brand_name")
    @Expose
    public String brandName;

    public boolean isWishList = false;

}
