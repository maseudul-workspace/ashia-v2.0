package com.webinfotech.ashia20.domain.interactors.impl;

import com.webinfotech.ashia20.domain.executors.Executor;
import com.webinfotech.ashia20.domain.executors.MainThread;
import com.webinfotech.ashia20.domain.interactors.FetchProductDetailsInteractor;
import com.webinfotech.ashia20.domain.interactors.base.AbstractInteractor;
import com.webinfotech.ashia20.domain.models.ProductDetailsData;
import com.webinfotech.ashia20.domain.models.ProductDetailsDataWrapper;
import com.webinfotech.ashia20.repository.AppRepositoryImpl;

public class FetchProductDetailsInteractorImpl extends AbstractInteractor implements FetchProductDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int productId;

    public FetchProductDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int productId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.productId = productId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(ProductDetailsData productDetailsData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductDetailsSuccess(productDetailsData);
            }
        });
    }

    @Override
    public void run() {
        ProductDetailsDataWrapper productDetailsDataWrapper = mRepository.fetchProductDetails(productId);
        if (productDetailsDataWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!productDetailsDataWrapper.status) {
            notifyError(productDetailsDataWrapper.message);
        } else {
            postMessage(productDetailsDataWrapper.productDetailsData);
        }
    }
}
