package com.webinfotech.ashia20.domain.interactors;

import com.webinfotech.ashia20.domain.models.Wishlist;

public interface FetchWishlistInteractor {
    interface Callback {
        void onWishlistFetchSuccess(Wishlist[] wishlists);
        void onWishlistFetchFail(String errorMsg, int loginError);
    }
}
