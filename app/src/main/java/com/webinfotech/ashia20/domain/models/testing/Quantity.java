package com.webinfotech.ashia20.domain.models.testing;

public class Quantity {
    public int id;
    public String qty;
    public boolean isSelected = false;

    public Quantity(int id, String qty, boolean isSelected) {
        this.id = id;
        this.qty = qty;
        this.isSelected = isSelected;
    }
}
