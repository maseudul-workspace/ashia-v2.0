package com.webinfotech.ashia20.domain.models.testing;

public class FilterValue {

    public int filterValueId;
    public String filterValue;
    public String color;
    public int fiterType;
    public boolean isSelected;
    public int filterNameId;

    public FilterValue(int filterValueId, int filterNameId, String filterValue, String color, int fiterType, boolean isSelected) {
        this.filterValueId = filterValueId;
        this.filterValue = filterValue;
        this.color = color;
        this.fiterType = fiterType;
        this.isSelected = isSelected;
        this.filterNameId = filterNameId;
    }

}
