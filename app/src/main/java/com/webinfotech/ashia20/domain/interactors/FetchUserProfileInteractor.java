package com.webinfotech.ashia20.domain.interactors;


import com.webinfotech.ashia20.domain.models.UserInfo;

public interface FetchUserProfileInteractor {
    interface Callback {
        void onGettingUserProfileSuccess(UserInfo userInfo);
        void onGettingUserProfileFail(String errorMsg, int loginError);
    }
}
