package com.webinfotech.ashia20.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductListWithFilterWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("total_page")
    @Expose
    public int totalPage;

    @SerializedName("current_page")
    @Expose
    public String current_page;

    @SerializedName("data")
    @Expose
    public ProductListData productListData;

}
