package com.webinfotech.ashia20;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.webinfotech.ashia20.domain.models.FilterName;
import com.webinfotech.ashia20.domain.models.PriceRange;
import com.webinfotech.ashia20.domain.models.UserInfo;
import com.webinfotech.ashia20.domain.models.Wishlist;


public class AndroidApplication extends Application {

    UserInfo userInfo;
    FilterName[] filterNames;
    PriceRange priceRange;
    Wishlist[] wishlists;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setUserInfo(Context context, UserInfo userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                "ASHIA", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString("USER", new Gson().toJson(userInfo));
        } else {
            editor.putString("USER", "");
        }

        editor.commit();
    }

    public UserInfo getUserInfo(Context context){
        UserInfo user;
        if(userInfo != null){
            user = userInfo;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    "ASHIA", Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString("USER","");
            if(userInfoJson.isEmpty()){
                user = null;
            }else{
                try {
                    user = new Gson().fromJson(userInfoJson, UserInfo.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

    public void setFilterNames(FilterName[] filterNames) {
        this.filterNames = filterNames;
    }

    public FilterName[] getFilterNames() {
        return this.filterNames;
    }

    public void setPriceRange(PriceRange priceRange) {
        this.priceRange = priceRange;
    }

    public PriceRange getPriceRange() {
        return this.priceRange;
    }

    public void setWishlists(Wishlist[] wishlists) {
        this.wishlists = wishlists;
    }

    public Wishlist[] getWishlists() {
        return this.wishlists;
    }

}
